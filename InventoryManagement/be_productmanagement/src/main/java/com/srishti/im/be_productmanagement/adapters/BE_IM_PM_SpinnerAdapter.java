package com.srishti.im.be_productmanagement.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.srishti.im.be_productmanagement.R;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Log;

/**
 * This adapter is for the spinners
 */

public class BE_IM_PM_SpinnerAdapter extends ArrayAdapter {

    private String[] values=null;
    private int layoutId=0;
    private Context context=null;

    /**
     * This is the constructor which must be used to get the obejct of the adapter
     * @param context context
     * @param resource layout using which the spinner must be populated
     * @param values values which is used to populate the spinner
     */
    public BE_IM_PM_SpinnerAdapter(@NonNull Context context, @LayoutRes @NonNull int resource,@NonNull String[] values) {
        super(context, resource);
        this.values = values;
        this.layoutId = resource;
        this.context = context;
    }

    @Override
    public int getCount() {
        return values.length;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //Get the spinner layout
        View view = LayoutInflater.from(context).inflate(this.layoutId,null);
        TextView textView = (TextView) view.findViewById(R.id.T_SL_textView);
        textView.setText(values[position]);
        if(position==0){
            textView.setTextColor(context.getResources().getColor(R.color.grey));
        }
        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //Get the spinner layout
        View view = LayoutInflater.from(context).inflate(this.layoutId,null);
        TextView textView = (TextView) view.findViewById(R.id.T_SL_textView);
        textView.setText(values[position]);
        if(position==0){
            textView.setTextColor(context.getResources().getColor(R.color.grey));
        }
        BE_IM_PM_Log.getInstance().debug(values[position]);
        return view;
    }
}
