package com.srishti.im.be_productmanagement.db_middleware;

import android.content.Context;
import android.widget.Toast;

import com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Log;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Resources;
import com.srishtiesdm.spdbinterfacelib.SP_DBInterfaceMain;
import com.srishtiesdm.spdbinterfacelib.entity.SIDB_CategoryTableEntity;
import com.srishtiesdm.spdbinterfacelib.entity.SIDB_GSTEntity;
import com.srishtiesdm.spdbinterfacelib.entity.SIDB_MasterTableEntity;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.addAProductKeyArray;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_CAT_DESCRIPTION;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_CAT_ID;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_CAT_LEVEL;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_CAT_NAME;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_PRE_CAT_ID;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.viewAProductKeyArray;

/**
 * This class acts as interface between the DB APIs and the application layer
 */

public class BE_IM_PM_DBInterface {
    //Singleton onstruct
    private static BE_IM_PM_DBInterface be_im_pm_dbInterface=null;
    private BE_IM_PM_Log log=null;
    private Context context=null;
    private boolean dataBaseInitSuccess=false;

    /**
     * This method returns the singleton instance of this class.
     * @return BE_IM_PM_DBInterface
     */
    public static BE_IM_PM_DBInterface getInstance(Context context){
        if(be_im_pm_dbInterface== null){
            be_im_pm_dbInterface = new BE_IM_PM_DBInterface(context);
        }
        return be_im_pm_dbInterface;
    }

    /**
     * Default constructror
     */
    public BE_IM_PM_DBInterface(Context context) {
        //get the LOG class instance
        log = BE_IM_PM_Log.getInstance();
        try {
            //get the DB APIs class instance
            sp_dbInterfaceMain =new SP_DBInterfaceMain(context);
            sp_dbInterfaceMain.initializeSPDatabase();
            dataBaseInitSuccess=true;
        }catch (Exception e){
            log.debug("Data base initialisation failed"+e.getMessage());
            e.printStackTrace();
            Toast error =new Toast(context);
            error.setText(e.getMessage()+'\n'+BE_IM_PM_Resources.getInstance().pleaseRestartApplication);
            error.setDuration(Toast.LENGTH_LONG);
            error.show();
            dataBaseInitSuccess = false;
        }
        //Copy context
        this.context=context;
    }

    /**
     * This method is used to clear te singleton instance of this class.
     */
    public static void clearInstance(){
        if(be_im_pm_dbInterface !=null){
            be_im_pm_dbInterface = null;
        }
    }

    //DB API
    private SP_DBInterfaceMain sp_dbInterfaceMain=null;

    /**
     * This method returns all the measurement units.
     * @return String[]
     */
    public String[] getMeasurementUnits(){
        return BE_IM_PM_Constants.MEASURING_UNITS;
    }

    /**
     * This method returns the GST Ids.
     * @return
     */
    public ArrayList<HashMap<String,String>> getGstIds(){
        //Delay for proper opening and closing of the progress dialog
        getDelay(100);
        ArrayList<HashMap<String,String>> gstData = new ArrayList<>();
      /*  for(int i=0; i<BE_IM_PM_Constants.DUMMY_GST_CODES.length;i++){
            HashMap<String,String> gstDetails = new HashMap<>();
            gstDetails.put(BE_IM_PM_Constants.gstDummyIdKeys[0],BE_IM_PM_Constants.DUMMY_GST_CODES[i]);
            gstDetails.put(BE_IM_PM_Constants.gstDummyIdKeys[1],BE_IM_PM_Constants.DUMMY_GST_IDS[i]);
            gstData.add(gstDetails);
        }*/
        //If data base was not initialised properly return
        if(!dataBaseInitSuccess){
            return new ArrayList<>();
        }
        //get category data from the DB
        List<SIDB_GSTEntity> catDataFromDb = sp_dbInterfaceMain.getAllGSTData();
        //If data is null return a empty array list
        if(catDataFromDb==null){
            return new  ArrayList<>();
        }
        for(int i=0; i<catDataFromDb.size(); i++){
            HashMap<String,String> singleGstItem = new HashMap<>();
            singleGstItem.put(BE_IM_PM_Constants.gstDummyIdKeys[0],convertIntToString(catDataFromDb.get(i).getSIDB_GSTtaxcode()));
            singleGstItem.put(BE_IM_PM_Constants.gstDummyIdKeys[1],convertDoubleToString(catDataFromDb.get(i).getSIDB_CGSTRate()));
            gstData.add(singleGstItem);
        }
        log.debug("Category ID and Names "+gstData.toString());
        return gstData;
    }

    /**
     * This method returns the allowence consents
     * @return string[]
     */
    public String[] getAllowenceConsents(){
        //Delay for proper opening and closing of the progress dialog
        getDelay(100);
        return BE_IM_PM_Constants.ALLOWENCECONSENT;
    }

    /**
     * This method is called to get all the categories
     * @return Arraylist of hashmaps , each hash map corresponds to the row in the table
     */
    public ArrayList<HashMap<String,String>> getAllTheCategories(String previousCatId){
        //Delay for proper opening and closing of the progress dialog
        getDelay(100);
        // TODO while using the actual DB API comment the dummy cats and sub cats
        ArrayList<HashMap<String,String>> data = new ArrayList<>();
      /*
        if(previousCatId =="0") {
            //create dummy categories
            HashMap<String, String> cat1 = new HashMap<>();
            cat1.put(SNO, "1");
            cat1.put(sIDB_CAT_NAME, "Rice");
            cat1.put(sIDB_CAT_DESCRIPTION, "Can be boiled and eaten");
            cat1.put(sIDB_CAT_LEVEL, "1");
            cat1.put(sIDB_CAT_ID, "11");
            cat1.put(sIDB_PRE_CAT_ID, "0");
            HashMap<String, String> cat2 = new HashMap<>();
            cat2.put(SNO, "2");
            cat2.put(sIDB_CAT_NAME, "milk");
            cat2.put(sIDB_CAT_DESCRIPTION, "Can be boiled and drunken");
            cat2.put(sIDB_CAT_LEVEL, "1");
            cat2.put(sIDB_CAT_ID, "12");
            cat2.put(sIDB_PRE_CAT_ID, "0");
            data.add(cat1);
            data.add(cat2);
        }else if(previousCatId == "11"){
            //create dummy categories
            HashMap<String, String> cat1 = new HashMap<>();
            cat1.put(SNO, "1");
            cat1.put(sIDB_CAT_NAME, "Brown");
            cat1.put(sIDB_CAT_DESCRIPTION, "Low glucamic index");
            cat1.put(sIDB_CAT_LEVEL, "2");
            cat1.put(sIDB_CAT_ID, "111");
            cat1.put(sIDB_PRE_CAT_ID, "11");
            HashMap<String, String> cat2 = new HashMap<>();
            cat2.put(SNO, "2");
            cat2.put(sIDB_CAT_NAME, "White");
            cat2.put(sIDB_CAT_DESCRIPTION, "Medium glucamic index , not recommended for sugar patients");
            cat2.put(sIDB_CAT_LEVEL, "2");
            cat2.put(sIDB_CAT_ID, "112");
            cat2.put(sIDB_PRE_CAT_ID, "11");
            HashMap<String, String> cat3 = new HashMap<>();
            cat3.put(SNO, "3");
            cat3.put(sIDB_CAT_NAME, "Biriyani");
            cat3.put(sIDB_CAT_DESCRIPTION, "High glucamic index , not recommended for sugar patients");
            cat3.put(sIDB_CAT_LEVEL, "2");
            cat3.put(sIDB_CAT_ID, "113");
            cat3.put(sIDB_PRE_CAT_ID, "11");
            data.add(cat1);
            data.add(cat2);
            data.add(cat3);
        }else if(previousCatId == "12"){
            //create dummy categories
            HashMap<String, String> cat1 = new HashMap<>();
            cat1.put(SNO, "1");
            cat1.put(sIDB_CAT_NAME, "Tetra pack");
            cat1.put(sIDB_CAT_DESCRIPTION, "6 months shelf life");
            cat1.put(sIDB_CAT_LEVEL, "2");
            cat1.put(sIDB_CAT_ID, "121");
            cat1.put(sIDB_PRE_CAT_ID, "12");
            HashMap<String, String> cat2 = new HashMap<>();
            cat2.put(SNO, "2");
            cat2.put(sIDB_CAT_NAME, "Packaged");
            cat2.put(sIDB_CAT_DESCRIPTION, "1 day of shelf life");
            cat2.put(sIDB_CAT_LEVEL, "2");
            cat2.put(sIDB_CAT_ID, "122");
            cat2.put(sIDB_PRE_CAT_ID, "12");
            data.add(cat1);
            data.add(cat2);
        }else if(previousCatId == "111"){
            //create dummy categories
            HashMap<String, String> cat1 = new HashMap<>();
            cat1.put(SNO, "1");
            cat1.put(sIDB_CAT_NAME, "Hanuman");
            cat1.put(sIDB_CAT_DESCRIPTION, "rice from sajeevini");
            cat1.put(sIDB_CAT_LEVEL, "3");
            cat1.put(sIDB_CAT_ID, "1111");
            cat1.put(sIDB_PRE_CAT_ID, "111");
            data.add(cat1);
        }else if(previousCatId == "112"){
            //create dummy categories
            HashMap<String, String> cat1 = new HashMap<>();
            cat1.put(SNO, "1");
            cat1.put(sIDB_CAT_NAME, "ramram");
            cat1.put(sIDB_CAT_DESCRIPTION, "rice from ujjaini");
            cat1.put(sIDB_CAT_LEVEL, "3");
            cat1.put(sIDB_CAT_ID, "1121");
            cat1.put(sIDB_PRE_CAT_ID, "112");
            data.add(cat1);
        }else if(previousCatId == "121"){
            //create dummy categories
            HashMap<String, String> cat1 = new HashMap<>();
            cat1.put(SNO, "1");
            cat1.put(sIDB_CAT_NAME, "Nandini");
            cat1.put(sIDB_CAT_DESCRIPTION, "regional , 3% fat");
            cat1.put(sIDB_CAT_LEVEL, "3");
            cat1.put(sIDB_CAT_ID, "1211");
            cat1.put(sIDB_PRE_CAT_ID, "121");
            HashMap<String, String> cat2 = new HashMap<>();
            cat2.put(SNO, "2");
            cat2.put(sIDB_CAT_NAME, "Amul");
            cat2.put(sIDB_CAT_DESCRIPTION, "National , 1.3% fat");
            cat2.put(sIDB_CAT_LEVEL, "3");
            cat2.put(sIDB_CAT_ID, "1212");
            cat2.put(sIDB_PRE_CAT_ID, "121");
            data.add(cat1);
            data.add(cat2);
        }else if(previousCatId == "1211"){
            //create dummy categories
            HashMap<String, String> cat1 = new HashMap<>();
            cat1.put(SNO, "1");
            cat1.put(sIDB_CAT_NAME, "500ml");
            cat1.put(sIDB_CAT_DESCRIPTION, "500ml");
            cat1.put(sIDB_CAT_LEVEL, "4");
            cat1.put(sIDB_CAT_ID, "12111");
            cat1.put(sIDB_PRE_CAT_ID, "1211");
            HashMap<String, String> cat2 = new HashMap<>();
            cat2.put(SNO, "2");
            cat2.put(sIDB_CAT_NAME, "1000ml");
            cat2.put(sIDB_CAT_DESCRIPTION, "1000ml");
            cat2.put(sIDB_CAT_LEVEL, "4");
            cat2.put(sIDB_CAT_ID, "12112");
            cat2.put(sIDB_PRE_CAT_ID, "1212");
            data.add(cat1);
            data.add(cat2);
        }else if(previousCatId == "12112"){
            //create dummy categories
            HashMap<String, String> cat1 = new HashMap<>();
            cat1.put(SNO, "1");
            cat1.put(sIDB_CAT_NAME, "green pack");
            cat1.put(sIDB_CAT_DESCRIPTION, "green pack");
            cat1.put(sIDB_CAT_LEVEL, "5");
            cat1.put(sIDB_CAT_ID, "121121");
            cat1.put(sIDB_PRE_CAT_ID, "12112");
            HashMap<String, String> cat2 = new HashMap<>();
            cat2.put(SNO, "2");
            cat2.put(sIDB_CAT_NAME, "red pack");
            cat2.put(sIDB_CAT_DESCRIPTION, "red pack");
            cat2.put(sIDB_CAT_LEVEL, "5");
            cat2.put(sIDB_CAT_ID, "121122");
            cat2.put(sIDB_PRE_CAT_ID, "12112");
            data.add(cat1);
            data.add(cat2);
        }*/

      //If data base was not initialised properly return
        if(!dataBaseInitSuccess){
            log.debug("Data base has not initialised properly");
            return new ArrayList<>();
        }
      //get category data from the DB
        List<SIDB_CategoryTableEntity> catDataFromDb = sp_dbInterfaceMain.getSIDBCategoryDetails(convertStringToInt(previousCatId));
        //If data is null return a empty array list
        if(catDataFromDb==null){
            return new ArrayList<>();
        }
        for(int i=0; i<catDataFromDb.size(); i++){
            HashMap<String,String> catHashMap = new HashMap<>();
            catHashMap.put(sIDB_CAT_ID,convertIntToString(catDataFromDb.get(i).getSIDB_CT_CatId()));
            catHashMap.put(sIDB_CAT_NAME,(catDataFromDb.get(i).getSIDB_CT_CatName()));
            catHashMap.put(sIDB_CAT_DESCRIPTION,catDataFromDb.get(i).getSIDB_CT_CatDescription());
            catHashMap.put(sIDB_CAT_LEVEL,convertIntToString(catDataFromDb.get(i).getSIDB_CT_CatLevel()));
            catHashMap.put(sIDB_PRE_CAT_ID,convertIntToString(catDataFromDb.get(i).getSIDB_CT_PreviousCatId()));
            log.debug("Category "+catHashMap.toString());
            data.add(catHashMap);
        }

        return data;
    }


    public HashMap<String,String> getAllCatNames(){
        //Delay for proper opening and closing of the progress dialog
        getDelay(100);
        HashMap<String,String> catNames = new HashMap<>();
        /*catNames.put("11","Rice");
        catNames.put("12","Milk");
        catNames.put("111","Brown");
        catNames.put("112","White");
        catNames.put("113","Biriyaani");
        catNames.put("121","TetraPack");
        catNames.put("122","Packaged");
        catNames.put("1111","Hanuman");
        catNames.put("1112","Ramram");
        catNames.put("1211","Nandini");
        catNames.put("1212","amul");
        catNames.put("12111","500ml");
        catNames.put("12112","1000ml");
        catNames.put("121121","Green");
        catNames.put("121122","Red");*/


        //If data base was not initialised properly return
        if(!dataBaseInitSuccess){
            return new HashMap<>();
        }
        //get category data from the DB
        List<SIDB_CategoryTableEntity> catDataFromDb = sp_dbInterfaceMain.getAllCategoryData();
        //If data is null return a empty array list
        if(catDataFromDb==null){
            return new  HashMap<>();
        }
        for(int i=0; i<catDataFromDb.size(); i++){
            catNames.put(convertIntToString(catDataFromDb.get(i).getSIDB_CT_CatId()),catDataFromDb.get(i).getSIDB_CT_CatName());
        }
        log.debug("Category ID and Names "+catNames.toString());
        return catNames;
    }

    /**
     * This method is used to add a product to the
     * prodcut table
     * @param productData hashmap , uses the addAProductKeyArray as key's
     * @return true- Success , false--failure
     */
    public boolean addAProduct(HashMap<String,String> productData){
        //Delay for proper opening and closing of the progress dialog
        getDelay(100);
     /* for(int i=0; i<productData.size();i++){
          //Log all the hashmap values
          log.debug(productData.get(addAProductKeyArray[i]));
      }
      // TODO to induce delay
        getDelay(2000);*/

     //Actuall DB Implementation
    //If data base was not initialised properly return
    if(!dataBaseInitSuccess){
        log.debug("Data base has not initialised properly");
        return false;
    }
    //Get the product data
    SIDB_MasterTableEntity productEntity = new SIDB_MasterTableEntity();
    productEntity.setSidb_Master_PartNn(productData.get(addAProductKeyArray[0]));
    productEntity.setSidb_Master_HSNNumber(convertStringToInt(productData.get(addAProductKeyArray[1])));
    productEntity.setSidb_Master_Categ(convertStringToInt(productData.get(addAProductKeyArray[2])));
    productEntity.setSidb_Master_SubcatL1(convertStringToInt(productData.get(addAProductKeyArray[3])));
    productEntity.setSidb_Master_SubcatL2(convertStringToInt(productData.get(addAProductKeyArray[4])));
    productEntity.setSIDB_Master_SubcatL3(convertStringToInt(productData.get(addAProductKeyArray[5])));
    productEntity.setSIDB_Master_SubcatL4(convertStringToInt(productData.get(addAProductKeyArray[6])));
    productEntity.setSidb_Master_GSTTaxCode(convertStringToInt(productData.get(addAProductKeyArray[7])));
    productEntity.setSidb_Master_ProductName((productData.get(addAProductKeyArray[8])));
    productEntity.setSidb_Master_MeasuringUnit((productData.get(addAProductKeyArray[9])));
    productEntity.setSidb_Master_Stock(convertStringToDouble(productData.get(addAProductKeyArray[10])));
    productEntity.setSidb_Master_StockCLR(convertStringToDouble(productData.get(addAProductKeyArray[11])));
    productEntity.setSidb_Master_Barcode(convertStringToInt(productData.get(addAProductKeyArray[12])));
    productEntity.setSidb_Master_Description((productData.get(addAProductKeyArray[13])));
    productEntity.setSidb_Master_Returnable(convertStringToBoolean(productData.get(addAProductKeyArray[14])));
    productEntity.setSidb_Master_TaXResv(convertStringToInt(productData.get(addAProductKeyArray[15])));
    productEntity.setSidb_Master_TRDate(convertStringToDate(productData.get(addAProductKeyArray[16])));
    long ret = sp_dbInterfaceMain.insertSingleMasterDataInformation(productEntity);
    if(ret==0){
        log.debug("adding a product is success");
        return true;
    }else if(ret==211){
        log.debug("adding a product Problem in data");
        return false;
    }else if(ret==212){
        log.debug("adding a product Problem in database");
        return false;
    }else{
        log.debug("adding a product is success ret : "+ret);
        return true;
    }
    }

    /**
     * This method is used to update a product to the
     * prodcut table
     * @param productData hashmap , uses the addAProductKeyArray as key's
     * @return true- Success , false--failure
     */
    public boolean updateAProduct(HashMap<String,String> productData){
        //Delay for proper opening and closing of the progress dialog
        getDelay(100);
       /* for(int i=0; i<productData.size();i++){
            //Log all the hashmap values
            log.debug(productData.get(addAProductKeyArray[i]));
        }
        // TODO to induce delay
        getDelay(2000);*/
        //Actuall DB Implementation
        //If data base was not initialised properly return
        if(!dataBaseInitSuccess){
            log.debug("Data base has not initialised properly");
            return false;
        }
        //Get the product data
        SIDB_MasterTableEntity productEntity = new SIDB_MasterTableEntity();
        productEntity.setSidb_Master_PartNn(productData.get(addAProductKeyArray[0]));
        productEntity.setSidb_Master_HSNNumber(convertStringToInt(productData.get(addAProductKeyArray[1])));
        productEntity.setSidb_Master_Categ(convertStringToInt(productData.get(addAProductKeyArray[2])));
        productEntity.setSidb_Master_SubcatL1(convertStringToInt(productData.get(addAProductKeyArray[3])));
        productEntity.setSidb_Master_SubcatL2(convertStringToInt(productData.get(addAProductKeyArray[4])));
        productEntity.setSIDB_Master_SubcatL3(convertStringToInt(productData.get(addAProductKeyArray[5])));
        productEntity.setSIDB_Master_SubcatL4(convertStringToInt(productData.get(addAProductKeyArray[6])));
        productEntity.setSidb_Master_GSTTaxCode(convertStringToInt(productData.get(addAProductKeyArray[7])));
        productEntity.setSidb_Master_ProductName((productData.get(addAProductKeyArray[8])));
        productEntity.setSidb_Master_MeasuringUnit((productData.get(addAProductKeyArray[9])));
        productEntity.setSidb_Master_Stock(convertStringToDouble(productData.get(addAProductKeyArray[10])));
        productEntity.setSidb_Master_StockCLR(convertStringToDouble(productData.get(addAProductKeyArray[11])));
        productEntity.setSidb_Master_Barcode(convertStringToInt(productData.get(addAProductKeyArray[12])));
        productEntity.setSidb_Master_Description((productData.get(addAProductKeyArray[13])));
        productEntity.setSidb_Master_Returnable(convertStringToBoolean(productData.get(addAProductKeyArray[14])));
        productEntity.setSidb_Master_TaXResv(convertStringToInt(productData.get(addAProductKeyArray[15])));
        productEntity.setSidb_Master_TRDate(convertStringToDate(productData.get(addAProductKeyArray[16])));
        long ret = sp_dbInterfaceMain.updateSingleMasterData(productEntity);
        if(ret==0){
            log.debug("updating success");
            return true;
        }else if(ret==211){
            log.debug("updating a product Problem in data");
            return false;
        }else if(ret==212){
            log.debug("updating a product Problem in database");
            return false;
        }else{
            log.debug("updating success ret :"+ret);
            return true;
        }
    }

    /**
     * This method is used to delete a product
     * @param productKey
     * @return
     */
    public boolean deleteAProduct(String productKey){
        //Delay for proper opening and closing of the progress dialog
        getDelay(100);
        log.debug("deleting "+productKey);
        getDelay(2000);
        return true;
    }


    /**
     * This method returns delay in that thread
     * @param time in milliseconds
     */
    private void getDelay(long time){
        try{
            Thread.sleep(time);
        }catch (Exception e){

        }
    }

    /**
     * This method returns the product details
     * each hashmap is a product and the arraylist is array of such hashmaps
     * @return ArrayList<HashMap<String,String>> keys are defined in the constants
     */
    public ArrayList<HashMap<String,String>> getProducts(){
        //Delay for proper opening and closing of the progress dialog
        getDelay(100);
        ArrayList<HashMap<String,String>> productData = new ArrayList<>();
        /*final String[] viewAProductKeyArray={"partno","hsnnumber","category","subcat1","subcat2","subcat3",
                "subcat4","gsttaxcode","productname","measuringunit","stock","clearstock","barcode","description",
                "returnable","taxreserve", "trdate"};
        //Product 1
        //brown rice mangalore
        String[] brownRiceMangalore={"brown rice mangalore11111","001","11","111","1111","0","0","1","Brown rice mangalore",
                "Kg","56.7","50.4","001","This rice is grown in mangalore a fine quality rice eported to europe","false","",
                "02-02-1975"};
        //Product 2
        //brown rice kerala
        String[] brownRiceKerala={"brown rice kerala11112","002","11","111","1112","0","0","2","Brown rice kerala",
                "Kg","33","33","002","This rice is grown in kerala a fine quality rice eported to USA","false","",
                "04-04-1980"};
        //White rice rice beedar
        String[] whiteRiceBeedar={"whiterice rice beedar11211","003","11","112","1121","0","0","1","White rice beedar",
                "Kg","100","97","003","This rice is grown in beedar used across karnataka","false","",
                "31-06-1999"};
        //Milk nandini green 1000 ml pack
        String[] milkNandini1000={"Nandini milk green 1000ml12111","004","12","121","1211","12112","121121","2","Nandini milk green 1000ml",
                "pcs","250","250","004","KMF milk family pack","true","",
                "31-12-2015"};
        //milk amul red 1000ml pack
        String[] milkAmul1000={"Amul milk red 1000ml12112","005","12","121","1211","12112","121122","3","Amul milk red 1000ml",
                "pcs","250","250","005","Amul milk family pack","true","",
                "31-12-2015"};
        HashMap<String,String> product = new HashMap<>();
        //Add mangalore rice
        for(int i=0; i<BE_IM_PM_Constants.viewAProductKeyArray.length;i++){
            product.put(BE_IM_PM_Constants.viewAProductKeyArray[i],brownRiceMangalore[i]);
        }
        productData.add(product);
        product = new HashMap<>();
        //Add kerala rice
        for(int i=0; i<BE_IM_PM_Constants.viewAProductKeyArray.length;i++){
            product.put(BE_IM_PM_Constants.viewAProductKeyArray[i],brownRiceKerala[i]);
        }
        productData.add(product);
        product = new HashMap<>();
        //White rice beedar
        for(int i=0; i<BE_IM_PM_Constants.viewAProductKeyArray.length;i++){
            product.put(BE_IM_PM_Constants.viewAProductKeyArray[i],whiteRiceBeedar[i]);
        }
        productData.add(product);
        product = new HashMap<>();
        //Milk nandini green
        for(int i=0; i<BE_IM_PM_Constants.viewAProductKeyArray.length;i++){
            product.put(BE_IM_PM_Constants.viewAProductKeyArray[i],milkNandini1000[i]);
        }
        productData.add(product);
        product = new HashMap<>();
        //milk amul red 1000ml pack
        for(int i=0; i<BE_IM_PM_Constants.viewAProductKeyArray.length;i++){
            product.put(BE_IM_PM_Constants.viewAProductKeyArray[i],milkAmul1000[i]);
        }
        productData.add(product);
        //Introduce dummy delay
        getDelay(2000);*/

        //Get data from actual DB
        //If data base was not initialised properly return
        if(!dataBaseInitSuccess){
            log.debug("Data base has not initialised properly");
            return new ArrayList<>();
        }

        //get the products from db
        List<SIDB_MasterTableEntity> productDataFromDB = sp_dbInterfaceMain.getAllMasterTableData();
        if(productData==null){
            log.debug("null prodcut details");
            return new ArrayList<>();
        }

        for (int i=0; i<productDataFromDB.size();i++){
            HashMap<String,String> singleProductData = new HashMap<>();
            singleProductData.put(viewAProductKeyArray[0],productDataFromDB.get(i).getSidb_Master_PartNn());
            singleProductData.put(viewAProductKeyArray[1],convertIntToString(productDataFromDB.get(i).getSidb_Master_HSNNumber()));
            singleProductData.put(viewAProductKeyArray[2],convertIntToString(productDataFromDB.get(i).getSidb_Master_Categ()));
            singleProductData.put(viewAProductKeyArray[3],convertIntToString(productDataFromDB.get(i).getSidb_Master_SubcatL1()));
            singleProductData.put(viewAProductKeyArray[4],convertIntToString(productDataFromDB.get(i).getSidb_Master_SubcatL2()));
            singleProductData.put(viewAProductKeyArray[5],convertIntToString(productDataFromDB.get(i).getSIDB_Master_SubcatL3()));
            singleProductData.put(viewAProductKeyArray[6],convertIntToString(productDataFromDB.get(i).getSIDB_Master_SubcatL4()));
            singleProductData.put(viewAProductKeyArray[7],convertIntToString(productDataFromDB.get(i).getSidb_Master_GSTTaxCode()));
            singleProductData.put(viewAProductKeyArray[8],(productDataFromDB.get(i).getSidb_Master_ProductName()));
            singleProductData.put(viewAProductKeyArray[9],(productDataFromDB.get(i).getSidb_Master_MeasuringUnit()));
            singleProductData.put(viewAProductKeyArray[10],convertDoubleToString(productDataFromDB.get(i).getSidb_Master_Stock()));
            singleProductData.put(viewAProductKeyArray[11],convertDoubleToString(productDataFromDB.get(i).getSidb_Master_StockCLR()));
            singleProductData.put(viewAProductKeyArray[12],convertIntToString(productDataFromDB.get(i).getSidb_Master_Barcode()));
            singleProductData.put(viewAProductKeyArray[13],(productDataFromDB.get(i).getSidb_Master_Description()));
            singleProductData.put(viewAProductKeyArray[14],convertBooleanToString(productDataFromDB.get(i).isSidb_Master_Returnable()));
            singleProductData.put(viewAProductKeyArray[15],convertIntToString(productDataFromDB.get(i).getSidb_Master_TaXResv()));
            singleProductData.put(viewAProductKeyArray[16],convertDateToString(productDataFromDB.get(i).getSidb_Master_TRDate()));
            productData.add(singleProductData);
        }
        return productData;
    }


    public HashMap<String,String> getTheProdcutFromBarcode(String barCode){
        if(barCode.equalsIgnoreCase("221489")) {
            String[] milkAmul1000 = {"dummy10", "005", "0", "0", "0", "0", "0", "3", "dummy1",
                    "pcs", "250", "250", "221489", "this is to test the adding product without categories", "false", "",
                    "31-12-2015"};
            HashMap<String, String> product = new HashMap<>();
            for (int i = 0; i < BE_IM_PM_Constants.viewAProductKeyArray.length; i++) {
                product.put(BE_IM_PM_Constants.viewAProductKeyArray[i], milkAmul1000[i]);
            }
            log.debug(product.toString());
            return product;
        }
        return new HashMap<>();
    }

    /**
     * THis method is used to convert integer value to the string
     * @param val integer value
     * @return Integer converted to String
     */
    private String convertIntToString(int val){
        return String.valueOf(val);
    }

    /**
     * Thsi method will convert the double value to the string
     * @param val double value to be conevreted
     * @return string value
     */
    private String convertDoubleToString(double val){
        return String.valueOf(val);
    }

    /**
     * This method i sused to conver the  boolena value to the string
     * @param val boolean value which is to be converted to the string value
     * @return booela as string
     */
    private String convertBooleanToString(boolean val){
        if(val){
            return "true";
        }else{
            return "false";
        }
    }

    /**
     * This method is used to convert the date to string
     * format
     * @param val date fomrt which is to be conevreted
     * @return daye as string
     */
    private String convertDateToString(Date val){
        try{
            Format formatter = new SimpleDateFormat("yyyy-MM-dd");
            String s = formatter.format(val);
            return s;
        }catch (Exception e){
            return "1970-01-01";
        }
    }

    /**
     * This method converts the String val to Integer
     * @param val String value to be converted to integer
     * @return
     */
    private int convertStringToInt(String val){
        try {
            return Integer.parseInt(val);
        }catch (Exception e){
            return 0;
        }
    }

    /**
     * This method is used to convert the String val to double
     * @param val String value
     * @return Double value
     */
    private Double convertStringToDouble(String val){
        try{
            return Double.parseDouble(val);
        }catch (Exception e){
            return 0.0;
        }
    }

    /**
     * This method converts the string value to boolean
     * @param val String value to be convetred to boolean
     * @return boolean
     */
    private boolean convertStringToBoolean(String val){
        if(val==null){
            return false;
        }
        if(val.equalsIgnoreCase("true")){
            return true;
        }else{
            return false;
        }
    }

    /**
     * This method is used to convert the String value to the
     * date value
     * @param val date in string format
     * @return java date
     */
    private Date convertStringToDate(String val){

        try{
            return new SimpleDateFormat("yyyy-MM-dd").parse(val);
        }catch (Exception e){
            try {
                return new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01");
            }catch (Exception e1){
            }
        }
        return null;
    }




}
