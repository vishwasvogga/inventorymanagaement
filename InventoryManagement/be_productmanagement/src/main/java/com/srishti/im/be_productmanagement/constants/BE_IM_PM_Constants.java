package com.srishti.im.be_productmanagement.constants;

/**
 * This class contains all the constant values
 */

public class BE_IM_PM_Constants {
    public static final String[] MEASURING_UNITS={"pcs","kg","gm","ltr","ml","quintal","ton","gallon","lbs"};
    public static final String[] ALLOWENCECONSENT={"Allowed","Not Allowed"};
    public static final String[] DUMMY_GST_IDS={"0%","5%","12%","15%","18%","28%"};
    public static final String[] DUMMY_GST_CODES={"1","2","3","4","5","6"};
    public static final String[] gstDummyIdKeys={"GST CODE","GST VALUE"};

    public static final String LOG_TITTLE = "BE_IM_PM_LOG";
    public static boolean isLogEnabled = false;
    //Keys
    public static final String SNO="sno";
    public static final String sIDB_CAT_NAME="sidbcatname";
    public static final String sIDB_CAT_DESCRIPTION="sidpcatdescription";
    public static final String sIDB_CAT_LEVEL="sidbcatlevel";
    public static final String sIDB_CAT_ID="sidbcatid";
    public static final String sIDB_PRE_CAT_ID="sidbprecatlevel";
    public static final String sIDB_MAIN_CATS_EXTRACTION_CODE="0";
    public static final String sIDB_NOT_SELECTED="Not selected";
    public static final String[] addAProductKeyArray={"partno","hsnnumber","category","subcat1","subcat2","subcat3","subcat4"
    ,"gsttaxcode","productname","measuringunit","stock","clearstock","barcode","description","returnable","taxreserve","trdate"};
    public static final String[] viewAProductKeyArray={"Part no","HSN Number","Category","Subcat1","Subcat2","Subcat3","Subcat4"
            ,"GST tax code","Product name","Measuring unit","Stock","Clearstock","Barcode","Description","Returnable","Tax reserve","Taxreserve date",
    };

    //Default values
    public static final String DEAFULT_STOCK="0";
    public static final String DEAFULT_CLEAR_STOCK ="0";
    public static final String DEFAULT_DATE="01-01-1970";

    /**
     * Config keys
     */
    public static final String MODE_OF_VIEW_ACT = "mode of view activity";
    public static final String PRODUCT_DETAILS="productDetails";
    public static final String BARCODE= "barcode";
    public static final String DATA_TO_ADD_ACT="adat to add product";
    /**
     * Config values
     */
    public static final int VIEW=0;
    public static final int EDIT=1;
    public static final int DELETE=2;
    /**
     * Acctivity launch codes
     */
    public static final int LAUNCH_PRODUCT_DETAILS_ACT=2;
    public static final int LAUNCH_EDIT_PRODUCT_ACT=3;

}
