package com.srishti.im.be_productmanagement.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.srishti.im.be_productmanagement.R;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Log;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Resources;

import java.util.HashMap;

/**
 * This adapter is used for the recycle views
 * product list
 */

public class BE_IM_PM_productDetailsRecyclerAdapter extends RecyclerView.Adapter {

    private Context context=null;
    private HashMap<String,String> productData=null;
    private int layoutResourceId=0;
    private MyViewHolder vh=null;

    /**
     * This is the default constructor of this adapter
     * @param context context
     * @param productData product list!
     * @param layoutResourceId custom layout of the recycle view
     */
    public BE_IM_PM_productDetailsRecyclerAdapter(Context context, HashMap<String,String> productData,
                                            int layoutResourceId) {
        super();
        this.context =context;
        this.productData = productData;
        this.layoutResourceId = layoutResourceId;
        BE_IM_PM_Log.getInstance().debug("recycle view adapter construcor called");
    }


    @Override
    public int getItemCount() {
        int itemsSize=productData.size();
        BE_IM_PM_Log.getInstance().debug("Items size"+itemsSize);
        return itemsSize;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BE_IM_PM_Log.getInstance().debug("oncreate view holder");
        View v = LayoutInflater.from(context).inflate(layoutResourceId, null, false);
        vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BE_IM_PM_Log.getInstance().debug("onbinding view"+position);
        //set the data
        BE_IM_PM_Resources resources=BE_IM_PM_Resources.getInstance();
        vh.label.setText(resources.viewAProductKeyArray[position]);
        vh.value.setText(productData.get(resources.viewAProductKeyArray[position]));
    }

    /**
     * Inner class view holder which extends recycle view view holder
     */
    private class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView label,value=null;
        // init the item view's
        public MyViewHolder(View itemView) {
            super(itemView);
            //get the reference
            label =(TextView) itemView.findViewById(R.id.T_VPDR_label);
            value=(TextView)itemView.findViewById(R.id.T_VPDR_value);
        }
    }
}

