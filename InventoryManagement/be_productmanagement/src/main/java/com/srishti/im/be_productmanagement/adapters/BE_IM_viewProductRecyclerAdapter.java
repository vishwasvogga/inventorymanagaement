package com.srishti.im.be_productmanagement.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srishti.im.be_productmanagement.R;
import com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Log;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Resources;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This adapter is used for the recycle views
 * product list
 */

public class BE_IM_viewProductRecyclerAdapter extends RecyclerView.Adapter {

    private Context context=null;
    private ArrayList<HashMap<String,String>> productData=null;
    private int layoutResourceId=0;
    private MyViewHolder vh=null;
    private View.OnClickListener onClickListener=null;

    /**
     * This is the default constructor of this adapter
     * @param context context
     * @param productData product list!
     * @param layoutResourceId custom layout of the recycle view
     * @param onProductClickListener onclick listener of the item of the list
     */
    public BE_IM_viewProductRecyclerAdapter(Context context, ArrayList<HashMap<String,String>> productData,
                                            int layoutResourceId,@Nullable View.OnClickListener onProductClickListener) {
        super();
        this.context =context;
        this.productData = productData;
        this.layoutResourceId = layoutResourceId;
        this.onClickListener=onProductClickListener;
        BE_IM_PM_Log.getInstance().debug("recycle view adapter construcor called");
    }


    @Override
    public int getItemCount() {
        int itemsSize=productData.size();
        BE_IM_PM_Log.getInstance().debug("Items size"+itemsSize);
        return itemsSize;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BE_IM_PM_Log.getInstance().debug("oncreate view holder");
        View v = LayoutInflater.from(context).inflate(layoutResourceId, null, false);
        vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BE_IM_PM_Log.getInstance().debug("onbinding view"+position);
        BE_IM_PM_Log.getInstance().debug("products "+productData.get(position).get(BE_IM_PM_Constants.viewAProductKeyArray[8]));
        //set the data
        BE_IM_PM_Resources resources=BE_IM_PM_Resources.getInstance();

        //Set Fileds
        vh.name.setText(productData.get(position).get(BE_IM_PM_Constants.viewAProductKeyArray[8]));
        vh.desription.setText(productData.get(position).get(BE_IM_PM_Constants.viewAProductKeyArray[13]));
      //  vh.batch.setText(resources.batch+": " +productData.get(position).get(BE_IM_PM_Constants.viewAProductKeyArray[17]));
     //   vh.price.setText(resources.price+": " +productData.get(position).get(BE_IM_PM_Constants.viewAProductKeyArray[18]));
        vh.stock.setText(resources.stock+": " +productData.get(position).get(BE_IM_PM_Constants.viewAProductKeyArray[10])+productData.get(position).get(BE_IM_PM_Constants.viewAProductKeyArray[9]));

        //Set on click listener
        if(onClickListener!=null) {
            vh.baselayout.setOnClickListener(this.onClickListener);
        }
    }

    /**
     * Inner class view holder which extends recycle view view holder
     */
    private class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,desription,batch,stock,price=null;
        public LinearLayout baselayout=null;
        // init the item view's
        public MyViewHolder(View itemView) {
            super(itemView);
            //get the reference
            name =(TextView) itemView.findViewById(R.id.T_RP_productName);
            desription=(TextView) itemView.findViewById(R.id.T_RP_productDesc);
         //   batch=(TextView) itemView.findViewById(R.id.T_RP_batch);
            stock=(TextView) itemView.findViewById(R.id.T_RP_stock);
         //   price=(TextView)itemView.findViewById(R.id.T_RP_price);
            baselayout=(LinearLayout) itemView.findViewById(R.id.L_RP_baseLayout);
        }
    }
}
