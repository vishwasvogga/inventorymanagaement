package com.srishti.im.be_productmanagement.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.barcode.main.ScanBarcodeActivity;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.srishti.im.be_productmanagement.R;
import com.srishti.im.be_productmanagement.db_middleware.BE_IM_PM_DBInterface;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_AlertBuilder;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Log;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Resources;

import java.util.HashMap;

import static com.barcode.main.ScanBarcodeActivity.AUTOFOCUS_ENABLE;
import static com.barcode.main.ScanBarcodeActivity.GETRESULT;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.BARCODE;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.DATA_TO_ADD_ACT;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.PRODUCT_DETAILS;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.viewAProductKeyArray;

/**
 * Created by Srishti-Admin on 21-09-2017.
 */

public class BE_IM_PM_AddProductPrequel extends Activity {
    private BE_IM_PM_DBInterface be_im_pm_dbInterface=null;
    private LinearLayout baseLayout=null;
    private Button B_Scan,B_Skip,B_Back = null;
    private TextView T_Message = null;
    private BE_IM_PM_Resources resource=null;
    private Context context=null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_addproductprequel_layout,null);
        setContentView(baseLayout);
        intialise();
        Configure();
        addActionListners();
    }

    /**
     * This method is used to initialise the UI elements and the
     * objects
     */
    private void intialise(){
        context = this;
        B_Scan = (Button) baseLayout.findViewById(R.id.B_APP_Scan);
        B_Skip =(Button) baseLayout.findViewById(R.id.B_APP_Skip);
        T_Message =(TextView) baseLayout.findViewById(R.id.T_APP_Message);
        B_Back = (Button)baseLayout.findViewById(R.id.B_APP_Back);
        resource = BE_IM_PM_Resources.getInstance();
        be_im_pm_dbInterface = BE_IM_PM_DBInterface.getInstance(context);
    }

    /**
     * Configure
     */
    private void Configure(){
        T_Message.setText(resource.please_scan_the_product_barcode);
        B_Scan.setText(resource.scan);
        B_Skip.setText(resource.skip);
        B_Back.setText(resource.Back);
    }

    /**
     * Add the action listeners to the variou UI elements
     */
    private void addActionListners(){
        B_Skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedToAddProductAct(new Bundle());
            }
        });

        B_Scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanBarcode();
            }
        });

        B_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BE_IM_PM_AddProductPrequel.this.finish();
            }
        });
    }

    /**
     * This method is used to call add product activity
     * @param bundle bundle whih has the barcode
     */
    private void proceedToAddProductAct(final Bundle bundle){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent addAProduct= new Intent(context,BE_IM_PM_addProduct.class);
                addAProduct.putExtra(DATA_TO_ADD_ACT,bundle);
                startActivity(addAProduct);
            }
        });
    }

    /**
     * This metho dis used to call to edit activity
     * @param bundle bundle which contains the product details
     */
    private void proceedToEditAct(final Bundle bundle){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent editProduct= new Intent(context,BE_IM_PM_editProduct.class);
                editProduct.putExtra(PRODUCT_DETAILS,bundle);
                startActivity(editProduct);
            }
        });
    }

    /**
     * Get edit details bundle
     */
    private Bundle getEditDetailsBundle(HashMap<String,String> productDetailsHashmap){
        Bundle productDetails=new Bundle();
        for (int i=0;i<viewAProductKeyArray.length;i++){
            //Get the category names
            productDetails.putString(viewAProductKeyArray[i], productDetailsHashmap.get(viewAProductKeyArray[i]));
        }
        return productDetails;
    }


    /**
     * This method is used to scan the bar code
     */
    private void scanBarcode(){
        int barCodeRequestCode = 1000;
        Intent intent = new Intent(this, ScanBarcodeActivity.class);
        intent.putExtra(AUTOFOCUS_ENABLE,true);
        startActivityForResult(intent,barCodeRequestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1000){
            if(resultCode == CommonStatusCodes.SUCCESS){
                if(data!=null){
                    final Barcode barcode = data.getParcelableExtra(GETRESULT);
                    //Get the product details from the DB
                    //if product is present move to update
                    //if not present go to add product
                    BE_IM_PM_Log.getInstance().debug("Scan bar code success : "+barcode.rawValue);
                    Runnable getTheProductFromBarCode = new Runnable() {
                        @Override
                        public void run() {
                            HashMap<String,String> product = getProductFromDb(barcode.rawValue);
                            BE_IM_PM_Log.getInstance().debug("product Det in scan bar code"+product.toString());
                            String productName = product.get(viewAProductKeyArray[0]);
                            if(productName==null){
                                productName="";
                            }
                            //check if we have data in the product by quering name form hashmap
                            if(productName.equalsIgnoreCase("")){
                                BE_IM_PM_Log.getInstance().debug("Product not present");
                                //no data present
                                //go to add activity and pass the bar code
                                Bundle bundle = new Bundle();
                                bundle.putString(BARCODE,barcode.rawValue);
                                proceedToAddProductAct(bundle);

                            }else{
                                BE_IM_PM_Log.getInstance().debug("Product already present");
                                //Product already present
                                //give a pop up do you want edit beacuse product already present
                                BE_IM_PM_AlertBuilder alert = new BE_IM_PM_AlertBuilder(context);
                                alert.createAlertBuilder(resource.productAlreadyPresent,resource.doYouWantToEditProductDetails);
                                alert.addPositiveButton(resource.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Go to edit activity
                                        //get the bundle
                                        proceedToEditAct(getEditDetailsBundle(getProductFromDb(barcode.rawValue)));
                                    }
                                });
                                alert.addNegetiveButton(resource.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Exit activity
                                        //Do nothing
                                    }
                                });
                                alert.showDialogBox();
                            }
                        }
                    };
                    new Thread(getTheProductFromBarCode).start();

                }else{
                    BE_IM_PM_Log.getInstance().debug("Scan bar code failed ");
                    inCaseScanFailed();
                }
            }else{
                BE_IM_PM_Log.getInstance().debug("Scan bar code failed ");
                inCaseScanFailed();
            }
        }
    }

    /**
     * This method is called whener the bar code scan fails
     */
    private void inCaseScanFailed(){
        BE_IM_PM_AlertBuilder alert = new BE_IM_PM_AlertBuilder(context);
        alert.createAlertBuilder(resource.scan,resource.scanFailedTryAgain);
        alert.addPositiveButton(resource.scan, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                scanBarcode();
            }
        });
        alert.addNegetiveButton(resource.skipScan, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Skip to add prodcut
                proceedToAddProductAct(new Bundle());
            }
        });
        alert.showDialogBox();
    }

    /**
     * This method returns the prodcut details from the bar code
     * @return hasmap of the prodcut details
     */
    private HashMap<String,String> getProductFromDb(String barCode){
        return be_im_pm_dbInterface.getTheProdcutFromBarcode(barCode);
    }
}
