package com.srishti.im.be_productmanagement.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.srishti.im.be_productmanagement.R;
import com.srishti.im.be_productmanagement.adapters.BE_IM_viewProductRecyclerAdapter;
import com.srishti.im.be_productmanagement.adapters.LineDividerDecoration;
import com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants;
import com.srishti.im.be_productmanagement.db_middleware.BE_IM_PM_DBInterface;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_AlertBuilder;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_CloseKeyboard;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Log;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_ProgressDialog;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.DELETE;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.LAUNCH_EDIT_PRODUCT_ACT;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.LAUNCH_PRODUCT_DETAILS_ACT;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.MODE_OF_VIEW_ACT;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.VIEW;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.viewAProductKeyArray;

/**
 * Created by Srishti-Admin on 16-09-2017.
 */

public class BE_IM_PM_viewProduct extends Activity {
    //objects
    private  RecyclerView RE_products=null;
    private ImageView I_Backspace=null;
    private BE_IM_PM_Resources resources=null;
    private BE_IM_PM_ProgressDialog progressDialog=null;
    private BE_IM_PM_Log log=null;
    private BE_IM_PM_DBInterface dbInterface=null;
    private BE_IM_PM_AlertBuilder alertBuilder=null;
    private SearchView SE_Searchproducts=null;
    private LinearLayout baseLayout=null;
    private Context context=null;
    private int onClickListItemMode=VIEW;
    private ArrayList<HashMap<String,String>> productData =null;
    private BE_IM_viewProductRecyclerAdapter recyclerAdapter=null;
    private HashMap<String,String> catNames=null;
    private ArrayList<HashMap<String,String>> filteredProductData=null;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        //Set the layout
        baseLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.viewproductlayout,null);
        this.setContentView(baseLayout);
        //Initialise
        initialise();
        //Configure
        configure();
        //Ad action listners
        addActionListeners();
    }

    /**
     * This method is used to initialise all the elements
     */
    private void initialise(){
        context=this;
        RE_products = (RecyclerView)getViewById(R.id.RE_viewProducts,baseLayout);
        I_Backspace=(ImageView)getViewById(R.id.I_VP_arrowBackSpace,baseLayout);
        SE_Searchproducts=(SearchView) getViewById(R.id.SE_VP_searchProduct,baseLayout);
        resources = BE_IM_PM_Resources.getInstance();
        progressDialog = new BE_IM_PM_ProgressDialog(context);
        alertBuilder = new BE_IM_PM_AlertBuilder(context);
        dbInterface = BE_IM_PM_DBInterface.getInstance(context);
        log = BE_IM_PM_Log.getInstance();
    }

    /**
     * This method retruns the view by id in the base layout
     * @param id id of that perticular view element
     * @param baseLayout this is the base layout in which the views are positioned
     * @return instance of the view
     */
    private View getViewById(int id, View baseLayout){
        return baseLayout.findViewById(id);
    }

    /**
     * This method is used to configure th evarious objects
     */
    private void configure(){
        //Deiconise searh view
        SE_Searchproducts.setIconified(false);
        ((EditText) SE_Searchproducts.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHintTextColor(Color.GRAY);
        ((EditText) SE_Searchproducts.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setTextColor(Color.BLACK);
        SE_Searchproducts.setQueryHint(resources.searchProduct);
        //Get the mode VIEW only or edit
        Intent input = getIntent();
        if(input!=null){
            Bundle data = input.getBundleExtra(BE_IM_PM_Constants.MODE_OF_VIEW_ACT);
            if(data!=null){
                onClickListItemMode = data.getInt(MODE_OF_VIEW_ACT);
            }
        }

        //Get all the product and cat data
        Runnable getProductData = new Runnable() {
            @Override
            public void run() {
                progressDialog.createProgressDialog(resources.Loading,resources.Please_Wait);
                //Get products data
                productData = dbInterface.getProducts();
                filteredProductData = productData;
                catNames = dbInterface.getAllCatNames();
                progressDialog.dismissProgressDialog();
                showListProductInitially();
            }
        };
        runOnThread(getProductData);
    }

    /**
     * This method is used to run a runnable on a back ground thread.
     * @param runnable
     */
    private void runOnThread(Runnable runnable){
        new Thread(runnable).start();
    }

    /**
     * This method is used to add onclick listeners
     * to the various UI elements
     */
    private void addActionListeners(){
        I_Backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitModule();
            }
        });

        SE_Searchproducts.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(recyclerAdapter!=null){
                    recyclerAdapter = new BE_IM_viewProductRecyclerAdapter(context,
                            filterSearch(query,false,productData),R.layout.recycleproductviewlayout,onclickProductItem);
                    RE_products.setAdapter(recyclerAdapter);

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(recyclerAdapter!=null){
                    recyclerAdapter = new BE_IM_viewProductRecyclerAdapter(context,
                            filterSearch(newText,false,productData),R.layout.recycleproductviewlayout,onclickProductItem);
                    RE_products.setAdapter(recyclerAdapter);
                }

                return false;
            }
        });

    }

    /**
     * This method is used to show the prodcusts on the recycle view
     */
    private void showListProduct(){
        Runnable showListProduct = new Runnable() {
            @Override
            public void run() {
                 recyclerAdapter = new BE_IM_viewProductRecyclerAdapter(context,
                        productData,R.layout.recycleproductviewlayout,onclickProductItem);
                // set a LinearLayoutManager with default horizontal orientation and false value for reverseLayout to show the items from start to end
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                RE_products.setLayoutManager(linearLayoutManager);
                RE_products.addItemDecoration(new LineDividerDecoration(context));
                RE_products.setAdapter(recyclerAdapter);
            }
        };
      runOnUiThread(showListProduct);
    }

    private void showListProductInitially(){
        Runnable showListProduct = new Runnable() {
            @Override
            public void run() {
                //If items are entered in the search view by default
                //Then filter recycle view
                String query = SE_Searchproducts.getQuery().toString();
                if(query.equalsIgnoreCase("")) {
                    recyclerAdapter = new BE_IM_viewProductRecyclerAdapter(context,
                            productData,R.layout.recycleproductviewlayout,onclickProductItem);
                }else{
                    recyclerAdapter = new BE_IM_viewProductRecyclerAdapter(context,
                            filterSearch(query,false,productData),R.layout.recycleproductviewlayout,onclickProductItem);
                }
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                RE_products.setLayoutManager(linearLayoutManager);
                RE_products.addItemDecoration(new LineDividerDecoration(context));
                RE_products.setAdapter(recyclerAdapter);
            }
        };
        runOnUiThread(showListProduct);
    }

    /**
     * This method will filter the list items by search text
     * @param searchText filtering text
     */
    public ArrayList<HashMap<String,String>> filterSearch(String searchText,boolean addAll,ArrayList<HashMap<String,String>> productList){
        ArrayList<HashMap<String,String>> tempproductData = new ArrayList<>();
        if(!addAll){
            for(int i=0; i<productList.size();i++){
                String searchQueryText = searchText.toLowerCase(Locale.getDefault());
                String productName = productList.get(i).get(BE_IM_PM_Constants.viewAProductKeyArray[8]).toLowerCase(Locale.getDefault());
                if(productName.contains(searchQueryText)){
                    tempproductData.add(productList.get(i));
                }
                BE_IM_PM_Log.getInstance().debug("searchText: "+searchQueryText+"  "+"productName:"+productName);
            }
        }else{
            tempproductData = productList;
        }
        //Save in a variable
        filteredProductData = tempproductData;
        return tempproductData;
    }

    /**
     * This is the onclick listner to be set to the
     * adapter to get the product item click event
     */
    View.OnClickListener onclickProductItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           final int itemIdClicked=RE_products.getChildLayoutPosition(v);
            //Check the mode
            if(onClickListItemMode==BE_IM_PM_Constants.EDIT){
                //If it is edit then redirect to edit activity
                //Pass the hash map of clicked product
                log.debug("Product item clicked EDIT mode"+itemIdClicked);
                Intent viewProductDetails = new Intent(context,BE_IM_PM_editProduct.class);
                viewProductDetails.putExtra(BE_IM_PM_Constants.PRODUCT_DETAILS, getEditDetailsBundle(itemIdClicked));
                startActivityForResult(viewProductDetails,LAUNCH_EDIT_PRODUCT_ACT);

            }else if(onClickListItemMode==BE_IM_PM_Constants.VIEW){
                // Redirect to the view prodcuct details
                //Pass the hashmap of clicked product
                log.debug("Product item clicked VIEW mode"+itemIdClicked);
                Intent viewProductDetails = new Intent(context,BE_IM_PM_viewProductDetails.class);
                viewProductDetails.putExtra(BE_IM_PM_Constants.PRODUCT_DETAILS,getProdcutDetailsBundle(itemIdClicked));
                startActivityForResult(viewProductDetails,LAUNCH_PRODUCT_DETAILS_ACT);
            }else if(onClickListItemMode == DELETE){
                //COnfirmation dialog box
                BE_IM_PM_AlertBuilder alertBuilder = new BE_IM_PM_AlertBuilder(context);
                alertBuilder.createAlertBuilder(resources.deleteProduct,resources.areYuSureTodelete);
                alertBuilder.addNegetiveButton(resources.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //DO nothing
                    }
                });
                alertBuilder.addPositiveButton(resources.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Yes sir , delete the product
                        //Delete the product
                        final String productId = filteredProductData.get(itemIdClicked).get(viewAProductKeyArray[0]);
                        Runnable deleteAProdcut = new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.createProgressDialog(resources.deleteProduct,resources.Please_Wait);
                                boolean ret =dbInterface.deleteAProduct(productId);
                                progressDialog.dismissProgressDialog();
                                if(!ret){
                                    BE_IM_PM_AlertBuilder alertBuilder = new BE_IM_PM_AlertBuilder(context);
                                    alertBuilder.createAlertBuilder(resources.deleteProduct,resources.prodcutDeletionFailed);
                                    alertBuilder.addNegetiveButton(resources.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Do  nothing
                                            //Let him attaempt once again!!
                                        }
                                    });
                                    alertBuilder.showDialogBox();

                                }else{
                                    BE_IM_PM_AlertBuilder alertBuilder = new BE_IM_PM_AlertBuilder(context);
                                    alertBuilder.createAlertBuilder(resources.deleteProduct,resources.productDeleted);
                                    alertBuilder.addNegetiveButton(resources.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Reload
                                            initialise();
                                            configure();
                                            addActionListeners();
                                        }
                                    });
                                    alertBuilder.showDialogBox();
                                }

                            }
                        };
                        runOnThread(deleteAProdcut);
                    }
                });
                alertBuilder.showDialogBox();

            }
        }
    };

    /**
     * This metjod returns the product details in a bundle
     */
    private Bundle getProdcutDetailsBundle(int itemClicked){
        Bundle productDetails=new Bundle();
        for (int i=0;i<viewAProductKeyArray.length;i++){
            //Get the category names
            if(i==2 || i==3 || i==4 || i==5|| i==6){
                productDetails.putString(viewAProductKeyArray[i], catNames.get(filteredProductData.get(itemClicked).get(viewAProductKeyArray[i])));
            }
            else {
                productDetails.putString(viewAProductKeyArray[i], filteredProductData.get(itemClicked).get(viewAProductKeyArray[i]));
            }
        }
        return productDetails;
    }

    /**
     * Get edit details bundle
     */
    private Bundle getEditDetailsBundle(int itemClicked){
        Bundle productDetails=new Bundle();
        for (int i=0;i<viewAProductKeyArray.length;i++){
            //Get the category names
            productDetails.putString(viewAProductKeyArray[i], filteredProductData.get(itemClicked).get(viewAProductKeyArray[i]));
        }
        return productDetails;
    }

    /**
     * This module will close the current activity and exit
     */
    private void exitModule(){
        // TODO cleared here
        BE_IM_PM_DBInterface.clearInstance();
        BE_IM_PM_Resources.clearInstance();
        BE_IM_PM_Log.clearInstance();
        new BE_IM_PM_CloseKeyboard().closeTheKeyBoard(context);
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //check for requet code
        //product details activity
        //Reload because recycle view corrupts while activity switching
        initialise();
        configure();
        addActionListeners();
        if(requestCode==LAUNCH_PRODUCT_DETAILS_ACT){
            if(resultCode==0){
                //Do nothing back pressed
            }else{
                //Exit pressed so finish current activity
                this.finish();
            }
        }
        if(requestCode==LAUNCH_EDIT_PRODUCT_ACT){
            if(resultCode==-1){
                //Do nothing back
            }else{

            }
        }
    }
}
