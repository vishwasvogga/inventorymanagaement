package com.srishti.im.be_productmanagement.utils;

/**
 * This class provides all the resources used in the modlule
 */

public class BE_IM_PM_Resources {
    private static BE_IM_PM_Resources be_im_pm_resources=null;

    /**
     * This method is used to get the instance of the resource class
     * @return BE_IM_PM_Resources
     */
    public static BE_IM_PM_Resources getInstance(){
        if(be_im_pm_resources == null){
            be_im_pm_resources = new BE_IM_PM_Resources();
        }
        return be_im_pm_resources;
    }

    /**
     * This method is used to clear teh existing sinlton instance
     */
    public static void clearInstance(){
        if(be_im_pm_resources!=null){
            be_im_pm_resources = null;
        }
    }

    public String Loading="Loading";
    public String Please_Wait = "Please wait";
    public String select_category="Select a category";
    public String select_subCategory1="Select a subcategory1";
    public String select_subCategory2="Select a subcategory2";
    public String select_subCategory3="Select a subcategory3";
    public String select_subCategory4="Select a subcategory4";
    public String select_measurement_unit="Select a measurement unit";
    public String select_gst_Id ="Select a GST ID";
    public String select_Replacement_Mode="Select replacement mode";
    public String pleaseEnterValidName="Enter valid name";
    public String pleaseEnterValidHSNNumber="Enter valid HSN number";
    public String pleaseSelectMeasurementUnit="Select a measurement unit";
    public String pleaseSelectGstCode="Select a GST code";
    public String pleaseSelectReplaceblePolicy="Select a replacable policy";
    public String pleaseSelectACategory="Select a category";
    public String pleaseSelectASubCategory1="Select a subcategory 1";
    public String pleaseSelectASubCategory2="Select a subcategory 2";
    public String pleaseSelectASubCategory3="Select a subcategory 3";
    public String pleaseSelectASubCategory4="Select a subcategory 4";
    public String productAddedSuccessfully="Adding success";
    public String updatingProduct="Updating product";
    public String productUpdatedSuccessfully="Product updated successfully!";
    public String productUpdationFailed="Product updation failed.";
    public String addingProductFailed="Adding failed";
    public String doYouWantToAddOneMore="Do you want to add one more?";
    public String doYouWantToGoBack="Do you want to go back?";
    public String yes="YES";
    public String no = "NO";
    public String ok="OK";
    public String addingProduct="Adding product";
    public String batch="Batch";
    public String stock="Stock";
    public String price="Price";
    public String searchProduct="Search product";
    public String[] viewAProductKeyArray={"Part no","HSN Number","Category","Subcat1","Subcat2","Subcat3","Subcat4"
            ,"GST tax code","Product name","Measuring unit","Stock","Clearstock","Barcode","Description","Returnable","Tax reserve","Taxreserve date",
            "Batch","Price"};
    public String update="UPDATE";
    public String updateProduct="Update product";
    public String areYuSureTodelete="Are you sure to delete?";
    public String productDeleted="Product has been deleted!";
    public String prodcutDeletionFailed="Product deletion failed.";
    public String deleteProduct="Delete prodcut";
    public String pleaseRestartApplication="Please restart the application!";
    public String please_scan_the_product_barcode = "Plaese scan the product bar code or skip.";
    public String skip="SKIP";
    public String scan="SCAN";
    public String scanFailedTryAgain = "Scan failed please tray again!";
    public String skipScan = "SKIP SCAN";
    public String productAlreadyPresent="Prodcut already present!";
    public String doYouWantToEditProductDetails="Do you want to edit product details?";
    public String Back="BACK";
}
