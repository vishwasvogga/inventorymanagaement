package com.srishti.im.be_productmanagement.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.srishti.im.be_productmanagement.R;
import com.srishti.im.be_productmanagement.adapters.BE_IM_PM_SpinnerAdapter;
import com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants;
import com.srishti.im.be_productmanagement.db_middleware.BE_IM_PM_DBInterface;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_AlertBuilder;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_CloseKeyboard;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_DisplayMessage;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Log;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_ProgressDialog;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Resources;

import java.util.ArrayList;
import java.util.HashMap;

import static android.view.View.GONE;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.BARCODE;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.DATA_TO_ADD_ACT;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.DEAFULT_CLEAR_STOCK;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.DEAFULT_STOCK;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.DEFAULT_DATE;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_CAT_ID;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_CAT_NAME;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_MAIN_CATS_EXTRACTION_CODE;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_NOT_SELECTED;

/**
 * This is the activity to be called for adding a product to the database.
 */

public class BE_IM_PM_addProduct extends Activity {
    //Object declarations
    private LinearLayout baseLayout=null;
    private EditText E_productName,E_productDescription,E_barcode,E_hsnNumber=null;
    private Spinner S_Category,S_SubCategory1,S_SubCategory2,S_SubCategory3,S_SubCategory4,S_MeasurementUnit,S_GSTId,S_IsReplaceble=null;
    private TextView T_LCategory,T_LSubcategory1,T_LSubcategory2,T_LSubcategory3,T_LSubcategory4,T_Lmeasurementunit,T_LGstId,T_LIsReplaceble=null;
    private ImageView I_arrowDown,I_arrowBack=null;
    private ScrollView S_Scroll=null;
    private TextView T_MessageView=null;
    private Button B_Add=null;
    private ArrayList<HashMap<String,String>> itemsCategory,itemsSubcategory1,itemsSubcategory2,itemsSubcategory3,itemsSubcategory4=null;
    private String[] itemsGstId,itemsMeasurementUnits,itemsReplacable=null;
    private BE_IM_PM_DBInterface dbInterface=null;
    private BE_IM_PM_Resources resources=null;
    private BE_IM_PM_Log log = null;
    private String lastSelectedPreCat=sIDB_NOT_SELECTED;
    private Context context=null;
    private BE_IM_PM_DisplayMessage displayMessage =null;
    private ArrayList<HashMap<String,String>> gstdataHashList=null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        //Set the layout
        baseLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.inventorymanagementmain,null);
        this.setContentView(baseLayout);
        //Initialise
        initialise();
        //Configure
        configure();
        //Ad action listners
        addActionListeners();
    }

    /**
     * This method is used to initialise
     * all the variables and UI elements
     */
    private void initialise(){
        //Conetxt
        context = this;
        //Get the UI refrences
        E_productName = (EditText) getViewById(R.id.E_productName,baseLayout);
        E_productDescription=(EditText)getViewById(R.id.E_productDescription,baseLayout);
        E_barcode =(EditText)getViewById(R.id.E_barcode,baseLayout);
        E_hsnNumber=(EditText)getViewById(R.id.E_hsnNumber,baseLayout);
        S_Category =(Spinner)getViewById(R.id.S_Category,baseLayout);
        S_SubCategory1=(Spinner)getViewById(R.id.S_SubCategory1,baseLayout);
        S_SubCategory2=(Spinner)getViewById(R.id.S_SubCategory2,baseLayout);
        S_SubCategory3=(Spinner)getViewById(R.id.S_SubCategory3,baseLayout);
        S_SubCategory4=(Spinner)getViewById(R.id.S_SubCategory4,baseLayout);
        S_MeasurementUnit=(Spinner)getViewById(R.id.S_measurementUnit,baseLayout);
        S_GSTId=(Spinner)getViewById(R.id.S_GstId,baseLayout);
        S_IsReplaceble=(Spinner)getViewById(R.id.S_isReplaceble,baseLayout);
        I_arrowDown = (ImageView) getViewById(R.id.I_arrowDown,baseLayout);
        B_Add=(Button)getViewById(R.id.B_Add,baseLayout);
        T_MessageView = (TextView) getViewById(R.id.T_MessageLine,baseLayout);
        S_Scroll = (ScrollView) getViewById(R.id.S_Scroll,baseLayout);
        T_Lmeasurementunit=(TextView)getViewById(R.id.T_LmesaurentUnit,baseLayout);
        T_LGstId=(TextView)getViewById(R.id.T_LGSTId,baseLayout);
        T_LIsReplaceble=(TextView)getViewById(R.id.T_LisReplaceble,baseLayout);
        T_LCategory=(TextView)getViewById(R.id.T_LCategory,baseLayout);
        T_LSubcategory1=(TextView)getViewById(R.id.T_LSubcat1,baseLayout);
        T_LSubcategory2=(TextView)getViewById(R.id.T_LSubcat2,baseLayout);
        T_LSubcategory3=(TextView)getViewById(R.id.T_LSubcat3,baseLayout);
        T_LSubcategory4=(TextView)getViewById(R.id.T_LSubcat4,baseLayout);
        I_arrowBack = (ImageView)getViewById(R.id.I_arrowBackSpace,baseLayout);

        //Get object instamces
        // TODO clear this instance at exit
        dbInterface = BE_IM_PM_DBInterface.getInstance(context);
        resources = BE_IM_PM_Resources.getInstance();
        log = BE_IM_PM_Log.getInstance();
        displayMessage = new BE_IM_PM_DisplayMessage(context);
        itemsGstId = new String[0];
        itemsMeasurementUnits = new String[0];
        itemsReplacable = new String[0];
        itemsCategory = new ArrayList<>();
        itemsSubcategory1 = new ArrayList<>();
        itemsSubcategory2 = new ArrayList<>();
        itemsSubcategory3 = new ArrayList<>();
        itemsSubcategory4 = new ArrayList<>();

    }

    /**
     * This method retruns the view by id in the base layout
     * @param id id of that perticular view element
     * @param baseLayout this is the base layout in which the views are positioned
     * @return instance of the view
     */
    private View getViewById(int id,View baseLayout){
        return baseLayout.findViewById(id);
    }

    /**
     * This method is used to configure the elements
     */
    private void configure(){
        //Check if bar code is paased if passed then set that to edit box
        Bundle bundle = getIntent().getBundleExtra(DATA_TO_ADD_ACT);
        if(bundle!=null){
            String barCode = bundle.getString(BARCODE);
            if(barCode !=null && !barCode.equalsIgnoreCase("")){
                E_barcode.setText(barCode);
            }
        }
        //Runnable for the initial loading of spinners
        Runnable loadSpinners = new Runnable() {
            @Override
            public void run() {
                //progress dialog
                final BE_IM_PM_ProgressDialog be_im_PM_progressDialog = new BE_IM_PM_ProgressDialog(context);
                be_im_PM_progressDialog.createProgressDialog(resources.Loading,resources.Please_Wait);
                //Get the Items to be set on spinner
                itemsMeasurementUnits = dbInterface.getMeasurementUnits();
                itemsReplacable = dbInterface.getAllowenceConsents();

                gstdataHashList = dbInterface.getGstIds();
                itemsGstId = new String[gstdataHashList.size()];
                for(int i=0; i<gstdataHashList.size();i++){
                    itemsGstId[i] = gstdataHashList.get(i).get(BE_IM_PM_Constants.gstDummyIdKeys[1]);
                }
                //Get the main categories
                itemsCategory = dbInterface.getAllTheCategories(sIDB_MAIN_CATS_EXTRACTION_CODE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Load the spinners
                        BE_IM_PM_SpinnerAdapter be_im_pm_spinnerAdapter;
                        String[] contents=null;
                        //Load the categories
                        contents = new String[itemsCategory.size()+1];
                        contents[0]=resources.select_category;
                        for(int i=1; i<=itemsCategory.size();i++){
                            contents[i]=itemsCategory.get(i-1).get(sIDB_CAT_NAME);
                            log.debug(contents[i]);
                        }
                        be_im_pm_spinnerAdapter = new BE_IM_PM_SpinnerAdapter(context,R.layout.spinner_layout,contents);
                        S_Category.setAdapter(be_im_pm_spinnerAdapter);

                        //Load measurement units
                        contents = new String[itemsMeasurementUnits.length+1];
                        contents[0]=resources.select_measurement_unit;
                        for(int i=1; i<=itemsMeasurementUnits.length;i++){
                            contents[i]=itemsMeasurementUnits[i-1];
                            log.debug(contents[i]);
                        }
                        be_im_pm_spinnerAdapter = new BE_IM_PM_SpinnerAdapter(context,R.layout.spinner_layout,contents);
                        S_MeasurementUnit.setAdapter(be_im_pm_spinnerAdapter);

                        be_im_PM_progressDialog.dismissProgressDialog();

                        //Load GST ID
                        contents = new String[itemsGstId.length+1];
                        contents[0] = resources.select_gst_Id;
                        for(int i=1; i<=itemsGstId.length;i++){
                            contents[i]=itemsGstId[i-1];
                            log.debug(contents[i]);
                        }
                        be_im_pm_spinnerAdapter = new BE_IM_PM_SpinnerAdapter(context,R.layout.spinner_layout,contents);
                        S_GSTId.setAdapter(be_im_pm_spinnerAdapter);

                        //Replacement
                        contents = new String[itemsReplacable.length+1];
                        contents[0] = resources.select_Replacement_Mode;
                        for(int i=1; i<=itemsReplacable.length;i++){
                            contents[i]=itemsReplacable[i-1];
                            log.debug(contents[i]);
                        }
                        be_im_pm_spinnerAdapter = new BE_IM_PM_SpinnerAdapter(context,R.layout.spinner_layout,contents);
                        S_IsReplaceble.setAdapter(be_im_pm_spinnerAdapter);
                        //Sub cat 1
                        contents = new String[1];
                        contents[0]=resources.select_subCategory1;
                        loadTheSpinner(S_SubCategory1,contents);
                        //Sub cat 2
                        contents = new String[1];
                        contents[0]=resources.select_subCategory2;
                        loadTheSpinner(S_SubCategory2,contents);
                        //Sub cat 3
                        contents = new String[1];
                        contents[0]=resources.select_subCategory3;
                        loadTheSpinner(S_SubCategory3,contents);
                        //Sub cat 4
                        contents = new String[1];
                        contents[0]=resources.select_subCategory4;
                        loadTheSpinner(S_SubCategory4,contents);
                    }
                });
            }
        };
        runOnThread(loadSpinners);
        //Configure display message
        displayMessage.configure(true,2000,100,false,T_MessageView);
        //Enable smooth scroll
        S_Scroll.setSmoothScrollingEnabled(true);
    }
    /**
     * this method is used to addActionListeners to the various UI elements
     */
    private void addActionListeners(){
        //Add action listeners to the category spinner
        S_Category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //check if category is selected or not
                if(position==0){
                    T_LCategory.setVisibility(GONE);
                    loadTheSpinner(S_SubCategory1,new String[]{resources.select_subCategory1});
                    return;
                }
                T_LCategory.setVisibility(View.VISIBLE);
                //Clear the subcat2,3,4
                itemsSubcategory2 = new ArrayList<HashMap<String, String>>();
                itemsSubcategory3 = new ArrayList<HashMap<String, String>>();
                itemsSubcategory4 = new ArrayList<HashMap<String, String>>();
                //Check the selected item and accordingly query subcat1
                String catId=itemsCategory.get(position-1).get(sIDB_CAT_ID);
                loadSpinnerValues(catId,1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        S_SubCategory1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //check if category is selected or not
                if(position==0){
                    //hide label
                    T_LSubcategory1.setVisibility(GONE);
                    //Load defualt in successive categories
                    loadTheSpinner(S_SubCategory2,new String[]{resources.select_subCategory2});
                    return;
                }
                T_LSubcategory1.setVisibility(View.VISIBLE);
                //Clear sub cat 3,4
                itemsSubcategory3 = new ArrayList<HashMap<String, String>>();
                itemsSubcategory4 = new ArrayList<HashMap<String, String>>();
                //Check the selected item and accordingly query subcat1
                String catId=itemsSubcategory1.get(position-1).get(sIDB_CAT_ID);
                loadSpinnerValues(catId,2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        S_SubCategory2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //check if category is selected or not
                if(position==0){
                    T_LSubcategory2.setVisibility(GONE);
                    loadTheSpinner(S_SubCategory3,new String[]{resources.select_subCategory3});
                    return;
                }
                T_LSubcategory2.setVisibility(View.VISIBLE);
                //Clear sub cat 4
                itemsSubcategory4 = new ArrayList<HashMap<String, String>>();
                //Check the selected item and accordingly query subcat1
                String catId=itemsSubcategory2.get(position-1).get(sIDB_CAT_ID);
                loadSpinnerValues(catId,3);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        S_SubCategory3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //check if category is selected or not
                if(position==0){
                    T_LSubcategory3.setVisibility(GONE);
                    loadTheSpinner(S_SubCategory4,new String[]{resources.select_subCategory4});
                    return;
                }
                T_LSubcategory3.setVisibility(View.VISIBLE);
                //Check the selected item and accordingly query subcat1
                String catId=itemsSubcategory3.get(position-1).get(sIDB_CAT_ID);
                loadSpinnerValues(catId,4);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //Spinner subcat 4
        S_SubCategory4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    T_LSubcategory4.setVisibility(GONE);
                    return;
                }
                T_LSubcategory4.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Spinner measuremnt
        S_MeasurementUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    T_Lmeasurementunit.setVisibility(GONE);
                }else
                {
                    T_Lmeasurementunit.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Spinner GST
        S_GSTId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    T_LGstId.setVisibility(GONE);
                }else
                {
                    T_LGstId.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Replace policy
        S_IsReplaceble.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    T_LIsReplaceble.setVisibility(GONE);
                }else
                {
                    T_LIsReplaceble.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Arrow down , scrol screen
        I_arrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Scroll the screen down by 0.75 of new page
                S_Scroll.smoothScrollBy(0,(int)(baseLayout.getMeasuredWidth()*0.20));
            }
        });

        //Arrow backspace
        I_arrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BE_IM_PM_AlertBuilder dialogBox = new BE_IM_PM_AlertBuilder(context);
                dialogBox.createAlertBuilder(resources.addingProduct,resources.doYouWantToGoBack);
                //Add one more product button
                DialogInterface.OnClickListener goBack= new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //i just wanna exit boy
                       exitModule();
                    }
                };
                dialogBox.addPositiveButton(resources.yes,goBack);
                //No i dont want to exit
                DialogInterface.OnClickListener dontwannaExit= new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //DO NOhting
                    }
                };
                dialogBox.addNegetiveButton(resources.no,dontwannaExit);
                dialogBox.showDialogBox();
            }
        });

        //SUbmit button
        B_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkFields()){
                    //Add the product to the product table
                    new BE_IM_PM_CloseKeyboard().closeTheKeyBoard(context);
                    WriteDataToDb();
                }
            }
        });


    }

    /**
     * This method is used to run a runnable on a back ground thread.
     * @param runnable
     */
    private void runOnThread(Runnable runnable){
        new Thread(runnable).start();
    }

    /**
     * THis method is used to load the spinners
     * @param catId category ID which is previous category of the current sub cat
     * @param subCatLevel 0-cat , 1-Sub cat1 , 4-Sub cat4
     */
    private void loadSpinnerValues(final String catId, final int subCatLevel){
        Runnable loadValues = new Runnable() {
            @Override
            public void run() {
                //progress dialog
                final BE_IM_PM_ProgressDialog be_im_PM_progressDialog = new BE_IM_PM_ProgressDialog(context);
                be_im_PM_progressDialog.createProgressDialog(resources.Loading,resources.Please_Wait);
                //Get the items of cat/sub cat
                String[] contents=null;
                Spinner spinner=null;
                if(subCatLevel==1){
                    itemsSubcategory1 = dbInterface.getAllTheCategories(catId);
                    //Load the sub cat 1
                    contents = new String[itemsSubcategory1.size()+1];
                    contents[0]=resources.select_subCategory1;
                    for(int i=1; i<=itemsSubcategory1.size();i++){
                        contents[i]=itemsSubcategory1.get(i-1).get(sIDB_CAT_NAME);
                        log.debug(contents[i]);
                    }
                    spinner = S_SubCategory1;

                }else if(subCatLevel==2){
                    itemsSubcategory2 = dbInterface.getAllTheCategories(catId);
                    //Load the sub cat 1
                    contents = new String[itemsSubcategory2.size()+1];
                    contents[0]=resources.select_subCategory2;
                    for(int i=1; i<=itemsSubcategory2.size();i++){
                        contents[i]=itemsSubcategory2.get(i-1).get(sIDB_CAT_NAME);
                        log.debug(contents[i]);
                    }
                    spinner = S_SubCategory2;

                }else if(subCatLevel==3){
                    itemsSubcategory3 = dbInterface.getAllTheCategories(catId);
                    //Load the sub cat 1
                    contents = new String[itemsSubcategory3.size()+1];
                    contents[0]=resources.select_subCategory3;
                    for(int i=1; i<=itemsSubcategory3.size();i++){
                        contents[i]=itemsSubcategory3.get(i-1).get(sIDB_CAT_NAME);
                        log.debug(contents[i]);
                    }
                    spinner = S_SubCategory3;
                }else if(subCatLevel==4){
                    itemsSubcategory4 = dbInterface.getAllTheCategories(catId);
                    //Load the sub cat 1
                    contents = new String[itemsSubcategory4.size()+1];
                    contents[0]=resources.select_subCategory4;
                    for(int i=1; i<=itemsSubcategory4.size();i++){
                        contents[i]=itemsSubcategory4.get(i-1).get(sIDB_CAT_NAME);
                        log.debug(contents[i]);
                    }
                    spinner = S_SubCategory4;
                }
                loadTheSpinner(spinner,contents);
                be_im_PM_progressDialog.dismissProgressDialog();
            }
        };
        runOnUiThread(loadValues);
    }

    /**
     * This method is used to laod the spinner
     * @param spinner soinner which is tobe loaded
     * @param contents contents of the spinner
     */
    private void loadTheSpinner(final Spinner spinner, final String[] contents){
        Runnable loadTheSpinner = new Runnable() {
            @Override
            public void run() {
                BE_IM_PM_SpinnerAdapter be_im_pm_spinnerAdapter=new BE_IM_PM_SpinnerAdapter(context,R.layout.spinner_layout,contents);
                if(spinner!=null){
                    spinner.setAdapter(be_im_pm_spinnerAdapter);
                }else{
                    log.debug("loading spinner"+"spinner is null");
                }
            }
        };
        runOnUiThread(loadTheSpinner);
    }

    /**
     * This module will check all the fileds before
     * adding the values to the database.
     * @return true/false , true-- proceed to adding else error message displayed on top
     */
    private boolean checkFields(){
        //Check the name
        String name = E_productName.getText().toString();
        if(name.equalsIgnoreCase("")){
           //throw error
            displayMessage.displayMessage(resources.pleaseEnterValidName, Color.RED);
            return false;
        }
        String hsnNumber=E_hsnNumber.getText().toString();
        //Check HSN
        if(hsnNumber.equalsIgnoreCase("")){
            //throw error
            displayMessage.displayMessage(resources.pleaseEnterValidHSNNumber, Color.RED);
            return false;
        }
        //Check measurement unit
        int selectedPos = S_MeasurementUnit.getSelectedItemPosition();
        int itemLength = itemsMeasurementUnits.length;
        if((itemLength!=0 && selectedPos==0)){
            displayMessage.displayMessage(resources.pleaseSelectMeasurementUnit,Color.RED);
            return false;
        }

        //Check GST id
        selectedPos = S_GSTId.getSelectedItemPosition();
        itemLength =itemsGstId.length;
        if((itemLength!=0 && selectedPos==0)){
            displayMessage.displayMessage(resources.pleaseSelectGstCode,Color.RED);
            return false;
        }

        //Check replacement mode
        selectedPos = S_IsReplaceble.getSelectedItemPosition();
        itemLength =itemsReplacable.length;
        if((itemLength!=0 && selectedPos==0)){
            displayMessage.displayMessage(resources.pleaseSelectReplaceblePolicy,Color.RED);
            return false;
        }

        //Check category
        selectedPos = S_Category.getSelectedItemPosition();
        itemLength =itemsCategory.size();
        if((itemLength!=0 && selectedPos==0)){
            displayMessage.displayMessage(resources.pleaseSelectACategory,Color.RED);
            return false;
        }

        //Check sub category 1
        selectedPos = S_SubCategory1.getSelectedItemPosition();
        itemLength =itemsSubcategory1.size();
        if((itemLength!=0 && selectedPos==0)){
            displayMessage.displayMessage(resources.pleaseSelectASubCategory1,Color.RED);
            return false;
        }

        //Check sub category 2
        selectedPos = S_SubCategory2.getSelectedItemPosition();
        itemLength =itemsSubcategory2.size();
        if((itemLength!=0 && selectedPos==0)){
            displayMessage.displayMessage(resources.pleaseSelectASubCategory2,Color.RED);
            return false;
        }

        //Check sub category 3
        selectedPos = S_SubCategory3.getSelectedItemPosition();
        itemLength =itemsSubcategory3.size();
        if((itemLength!=0 && selectedPos==0)){
            displayMessage.displayMessage(resources.pleaseSelectASubCategory3,Color.RED);
            return false;
        }

        //Check sub category 4
        selectedPos = S_SubCategory4.getSelectedItemPosition();
        itemLength =itemsSubcategory4.size();
        if((itemLength!=0 && selectedPos==0)){
            displayMessage.displayMessage(resources.pleaseSelectASubCategory4,Color.RED);
            return false;
        }

        return true;
    }

    /**
     * This method creates the hash map which is to be writen to the database
     */
    private void WriteDataToDb(){
        //Hashmap to hold the data
        final HashMap<String,String> product = new HashMap<>();
        String productName=E_productName.getText().toString();
        int mainCatID=S_Category.getSelectedItemPosition();
        int subCat1=S_SubCategory1.getSelectedItemPosition();
        int subCat2=S_SubCategory2.getSelectedItemPosition();
        int subCat3=S_SubCategory3.getSelectedItemPosition();
        int subCat4=S_SubCategory3.getSelectedItemPosition();
        //Prepare the product ID/Part number
        String partNumber=productName;
        partNumber=partNumber+mainCatID;
        if(subCat1!=0){
            partNumber=partNumber+subCat1;
        }
        if(subCat2!=0){
            partNumber=partNumber+subCat2;
        }
        if(subCat3!=0){
            partNumber=partNumber+subCat3;
        }
        if(subCat4!=0){
            partNumber=partNumber+subCat4;
        }
        //Add part number to the hashmap
        product.put(BE_IM_PM_Constants.addAProductKeyArray[0],partNumber);
        //Get and add hsn number
        product.put(BE_IM_PM_Constants.addAProductKeyArray[1],E_hsnNumber.getText().toString());
        //Add cat ID
        product.put(BE_IM_PM_Constants.addAProductKeyArray[2],String.valueOf(mainCatID));
        //Add sub cat1 ID
        product.put(BE_IM_PM_Constants.addAProductKeyArray[3],String.valueOf(subCat1));
        //Add sub cat2 ID
        product.put(BE_IM_PM_Constants.addAProductKeyArray[4],String.valueOf(subCat2));
        //Add sub cat3 ID
        product.put(BE_IM_PM_Constants.addAProductKeyArray[5],String.valueOf(subCat3));
        //Add sub cat4 ID
        product.put(BE_IM_PM_Constants.addAProductKeyArray[6],String.valueOf(subCat4));
        //GST Tax Code
        product.put(BE_IM_PM_Constants.addAProductKeyArray[7],String.valueOf(S_GSTId.getSelectedItemPosition()));
        //product name
        product.put(BE_IM_PM_Constants.addAProductKeyArray[8],E_productName.getText().toString());
        //Measuring unit
        product.put(BE_IM_PM_Constants.addAProductKeyArray[9],itemsMeasurementUnits[S_MeasurementUnit.getSelectedItemPosition()-1]);
        //Stock
        product.put(BE_IM_PM_Constants.addAProductKeyArray[10],DEAFULT_STOCK);
        //Clear stock
        product.put(BE_IM_PM_Constants.addAProductKeyArray[11], DEAFULT_CLEAR_STOCK);
        //Bar code
        product.put(BE_IM_PM_Constants.addAProductKeyArray[12],E_barcode.getText().toString());
        //Description
        String description=E_productDescription.getText().toString();
        if(description.length()>128){
            description = description.substring(0,127);
        }
        product.put(BE_IM_PM_Constants.addAProductKeyArray[13],description);
        //Replacement
        int isAllowed=1;
        if(itemsReplacable[S_IsReplaceble.getSelectedItemPosition()-1].equalsIgnoreCase("Allowed")){
            isAllowed=0;
        }
        product.put(BE_IM_PM_Constants.addAProductKeyArray[14],String.valueOf(isAllowed));
        //Tax reserve
        product.put(BE_IM_PM_Constants.addAProductKeyArray[15],String.valueOf(-1));
        //master tr date
        product.put(BE_IM_PM_Constants.addAProductKeyArray[16],DEFAULT_DATE);

        Runnable writeToDB =  new Runnable() {
            @Override
            public void run() {
                BE_IM_PM_ProgressDialog progressDialog = new BE_IM_PM_ProgressDialog(context);
                progressDialog.createProgressDialog(resources.addingProduct,resources.Please_Wait);
                //Write to DB
                boolean ret = dbInterface.addAProduct(product);
                progressDialog.dismissProgressDialog();
                if(ret){
                    //DB write success
                    displayMessage.displayMessage(resources.productAddedSuccessfully,Color.GREEN);
                    BE_IM_PM_AlertBuilder dialogBox = new BE_IM_PM_AlertBuilder(context);
                    dialogBox.createAlertBuilder(resources.addingProduct,resources.productAddedSuccessfully+resources.doYouWantToAddOneMore);
                    //Add one more product button
                    DialogInterface.OnClickListener okAddOneMore= new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Clear all the fields
                            E_productName.setText("");
                            E_productDescription.setText("");
                            E_barcode.setText("");
                            E_hsnNumber.setText("");
                            S_MeasurementUnit.setSelection(0);
                            S_GSTId.setSelection(0);
                            S_IsReplaceble.setSelection(0);
                            S_Category.setSelection(0);
                            //Scroll to starting
                            //Scroll the screen down by 0.75 of new page
                            S_Scroll.smoothScrollBy(0,-(int)(baseLayout.getMeasuredWidth()));
                            E_productName.requestFocus();
                        }
                    };
                    dialogBox.addPositiveButton(resources.yes,okAddOneMore);
                    //No i dont want to add , i just wanna exit boy
                    DialogInterface.OnClickListener wannaExit= new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Exit
                            exitModule();
                        }
                    };
                    dialogBox.addNegetiveButton(resources.no,wannaExit);
                    dialogBox.showDialogBox();
                }else{
                    //DB Write fail
                    displayMessage.displayMessage(resources.addingProductFailed,Color.RED);
                }
            }
        };
        runOnThread(writeToDB);
    }

    /**
     * This module will close the current activity and exit
     */
    private void exitModule(){
        // TODO cleared here
        BE_IM_PM_DBInterface.clearInstance();
        BE_IM_PM_Resources.clearInstance();
        BE_IM_PM_Log.clearInstance();
        new BE_IM_PM_CloseKeyboard().closeTheKeyBoard(context);
        this.finish();
    }


}
