package com.srishti.im.be_productmanagement.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * This class is used to build the
 * alert dialog box
 */

public class BE_IM_PM_AlertBuilder {
    private Context context=null;
    private AlertDialog.Builder alertDialog=null;

    /**
     * This is the public ocnstructor
     * @param context context
     */
    public BE_IM_PM_AlertBuilder(Context context) {
        this.context = context;
    }

    /**
     * This method is used to add a positive butotn to the
     * alert dialog box
     * @param positiveButton text on the button
     * @param onClickListener onclick listener of the positive button
     */
    public void addPositiveButton(String positiveButton,DialogInterface.OnClickListener onClickListener){
        alertDialog.setPositiveButton(positiveButton,onClickListener);
    }

    /**
     * This method is used to add a negetive butotn to the
     * alert dialog box
     * @param negetiveButton text on the button
     * @param onClickListener onclick listener of the positive button
     */
    public void addNegetiveButton(String negetiveButton,DialogInterface.OnClickListener onClickListener){
        alertDialog.setNegativeButton(negetiveButton,onClickListener);
    }

    /**
     * This method is used to create the dialog alert box
     * @param tittle tittle of the alert box
     * @param message message of te alert box
     */
    public void createAlertBuilder(String tittle,String message){
        alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        alertDialog.setTitle(tittle);
        alertDialog.setMessage(message);
    }

    /**
     * This method is used to show the dialog box
     */
    public void showDialogBox(){
        BE_IM_PM_Log.getInstance().debug("Showing the dialog box");
        Runnable showDialog = new Runnable() {
            @Override
            public void run() {
                BE_IM_PM_Log.getInstance().debug("Showing the dialog box-executing");
                alertDialog.show();
            }
        };
        runOnUiThread(showDialog);
    }

    /**
     * This method will run the runnables in the uI thread
     * @param runnable
     */
    private void runOnUiThread(Runnable runnable){
        ((Activity)context).runOnUiThread(runnable);
    }


}
