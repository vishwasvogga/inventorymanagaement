package com.srishti.im.be_productmanagement.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.srishti.im.be_productmanagement.R;
import com.srishti.im.be_productmanagement.adapters.BE_IM_PM_SpinnerAdapter;
import com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants;
import com.srishti.im.be_productmanagement.db_middleware.BE_IM_PM_DBInterface;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_AlertBuilder;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_CloseKeyboard;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_DisplayMessage;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Log;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_ProgressDialog;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Resources;

import java.util.ArrayList;
import java.util.HashMap;

import static android.view.View.GONE;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_CAT_ID;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_CAT_NAME;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_MAIN_CATS_EXTRACTION_CODE;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.sIDB_NOT_SELECTED;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.viewAProductKeyArray;

/**
 * This activity is to edit the
 */

public class BE_IM_PM_editProduct extends Activity {
    //Object declarations
    private LinearLayout baseLayout=null;
    private EditText E_productName,E_productDescription,E_barcode,E_hsnNumber=null;
    private Spinner S_Category,S_SubCategory1,S_SubCategory2,S_SubCategory3,S_SubCategory4,S_MeasurementUnit,S_GSTId,S_IsReplaceble=null;
    private TextView T_LCategory,T_LSubcategory1,T_LSubcategory2,T_LSubcategory3,T_LSubcategory4,T_Lmeasurementunit,T_LGstId,T_LIsReplaceble=null;
    private ImageView I_arrowDown,I_arrowBack=null;
    private ScrollView S_Scroll=null;
    private TextView T_MessageView=null;
    private Button B_Add=null;
    private ArrayList<HashMap<String,String>> itemsCategory,itemsSubcategory1,itemsSubcategory2,itemsSubcategory3,itemsSubcategory4=null;
    private String[] itemsGstId,itemsMeasurementUnits,itemsReplacable=null;
    private BE_IM_PM_DBInterface dbInterface=null;
    private BE_IM_PM_Resources resources=null;
    private BE_IM_PM_Log log = null;
    private String lastSelectedPreCat=sIDB_NOT_SELECTED;
    private Context context=null;
    private BE_IM_PM_DisplayMessage displayMessage =null;
    private HashMap<String,String> productDetails=null;
    private ArrayList<HashMap<String,String>> gstdataHashList=null;
    private HashMap<String,String> constantDataFromInput = new HashMap<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Set the layout
        baseLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.inventorymanagementmain,null);
        this.setContentView(baseLayout);
        //Initialise
        initialise();
        //Configure
        configure();
        //Ad action listners
        addActionListeners();
    }

    /**
     * This method is used to initialise
     * all the variables and UI elements
     */
    private void initialise(){
        //Conetxt
        context = this;
        //Get the UI refrences
        E_productName = (EditText) getViewById(R.id.E_productName,baseLayout);
        E_productDescription=(EditText)getViewById(R.id.E_productDescription,baseLayout);
        E_barcode =(EditText)getViewById(R.id.E_barcode,baseLayout);
        E_hsnNumber=(EditText)getViewById(R.id.E_hsnNumber,baseLayout);
        S_Category =(Spinner)getViewById(R.id.S_Category,baseLayout);
        S_SubCategory1=(Spinner)getViewById(R.id.S_SubCategory1,baseLayout);
        S_SubCategory2=(Spinner)getViewById(R.id.S_SubCategory2,baseLayout);
        S_SubCategory3=(Spinner)getViewById(R.id.S_SubCategory3,baseLayout);
        S_SubCategory4=(Spinner)getViewById(R.id.S_SubCategory4,baseLayout);
        S_MeasurementUnit=(Spinner)getViewById(R.id.S_measurementUnit,baseLayout);
        S_GSTId=(Spinner)getViewById(R.id.S_GstId,baseLayout);
        S_IsReplaceble=(Spinner)getViewById(R.id.S_isReplaceble,baseLayout);
        I_arrowDown = (ImageView) getViewById(R.id.I_arrowDown,baseLayout);
        B_Add=(Button)getViewById(R.id.B_Add,baseLayout);
        T_MessageView = (TextView) getViewById(R.id.T_MessageLine,baseLayout);
        S_Scroll = (ScrollView) getViewById(R.id.S_Scroll,baseLayout);
        T_Lmeasurementunit=(TextView)getViewById(R.id.T_LmesaurentUnit,baseLayout);
        T_LGstId=(TextView)getViewById(R.id.T_LGSTId,baseLayout);
        T_LIsReplaceble=(TextView)getViewById(R.id.T_LisReplaceble,baseLayout);
        T_LCategory=(TextView)getViewById(R.id.T_LCategory,baseLayout);
        T_LSubcategory1=(TextView)getViewById(R.id.T_LSubcat1,baseLayout);
        T_LSubcategory2=(TextView)getViewById(R.id.T_LSubcat2,baseLayout);
        T_LSubcategory3=(TextView)getViewById(R.id.T_LSubcat3,baseLayout);
        T_LSubcategory4=(TextView)getViewById(R.id.T_LSubcat4,baseLayout);
        I_arrowBack = (ImageView)getViewById(R.id.I_arrowBackSpace,baseLayout);

        //Get object instamces
        // TODO clear this instance at exit
        dbInterface = BE_IM_PM_DBInterface.getInstance(context);
        resources = BE_IM_PM_Resources.getInstance();
        log = BE_IM_PM_Log.getInstance();
        displayMessage = new BE_IM_PM_DisplayMessage(context);

        itemsGstId = new String[0];
        itemsMeasurementUnits = new String[0];
        itemsReplacable = new String[0];
        itemsCategory = new ArrayList<>();
        itemsSubcategory1 = new ArrayList<>();
        itemsSubcategory2 = new ArrayList<>();
        itemsSubcategory3 = new ArrayList<>();
        itemsSubcategory4 = new ArrayList<>();
    }

    /**
     * This method retruns the view by id in the base layout
     * @param id id of that perticular view element
     * @param baseLayout this is the base layout in which the views are positioned
     * @return instance of the view
     */
    private View getViewById(int id, View baseLayout){
        return baseLayout.findViewById(id);
    }

    /**
     * This method is used to configure the elements
     */
    private void configure(){
        //Disable name edit
        E_productName.setEnabled(false);
        S_Category.setEnabled(false);
        S_SubCategory1.setEnabled(false);
        S_SubCategory2.setEnabled(false);
        S_SubCategory3.setEnabled(false);
        S_SubCategory4.setEnabled(false);
        S_Category.setVisibility(GONE);
        S_SubCategory1.setVisibility(GONE);
        S_SubCategory2.setVisibility(GONE);
        S_SubCategory3.setVisibility(GONE);
        S_SubCategory4.setVisibility(GONE);
        T_LCategory.setVisibility(GONE);
        T_LSubcategory1.setVisibility(GONE);
        T_LSubcategory2.setVisibility(GONE);
        T_LSubcategory3.setVisibility(GONE);
        T_LSubcategory4.setVisibility(GONE);

        //change heading to update product
        T_MessageView.setText(resources.updateProduct);
        //Change add button to update
        B_Add.setText(resources.update);
        //Runnable for the initial loading of spinners
        Runnable loadSpinners = new Runnable() {
            @Override
            public void run() {
                //progress dialog
                final BE_IM_PM_ProgressDialog be_im_PM_progressDialog = new BE_IM_PM_ProgressDialog(context);
                be_im_PM_progressDialog.createProgressDialog(resources.Loading,resources.Please_Wait);
                //Get the Items to be set on spinner
                itemsMeasurementUnits = dbInterface.getMeasurementUnits();
                itemsReplacable = dbInterface.getAllowenceConsents();
                gstdataHashList = dbInterface.getGstIds();
                itemsGstId = new String[gstdataHashList.size()];
                for(int i=0; i<gstdataHashList.size();i++){
                    itemsGstId[i] = gstdataHashList.get(i).get(BE_IM_PM_Constants.gstDummyIdKeys[1]);
                }
                //Get the main categories
                itemsCategory = dbInterface.getAllTheCategories(sIDB_MAIN_CATS_EXTRACTION_CODE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Load the spinners
                        BE_IM_PM_SpinnerAdapter be_im_pm_spinnerAdapter;
                        String[] contents=null;
                        //Load the categories
                        contents = new String[itemsCategory.size()+1];
                        contents[0]=resources.select_category;
                        for(int i=1; i<=itemsCategory.size();i++){
                            contents[i]=itemsCategory.get(i-1).get(sIDB_CAT_NAME);
                            log.debug(contents[i]);
                        }
                        be_im_pm_spinnerAdapter = new BE_IM_PM_SpinnerAdapter(context,R.layout.spinner_layout,contents);
                        S_Category.setAdapter(be_im_pm_spinnerAdapter);

                        //Load measurement units
                        contents = new String[itemsMeasurementUnits.length+1];
                        contents[0]=resources.select_measurement_unit;
                        for(int i=1; i<=itemsMeasurementUnits.length;i++){
                            contents[i]=itemsMeasurementUnits[i-1];
                            log.debug(contents[i]);
                        }
                        be_im_pm_spinnerAdapter = new BE_IM_PM_SpinnerAdapter(context,R.layout.spinner_layout,contents);
                        S_MeasurementUnit.setAdapter(be_im_pm_spinnerAdapter);

                        be_im_PM_progressDialog.dismissProgressDialog();

                        //Load GST ID
                        contents = new String[itemsGstId.length+1];
                        contents[0] = resources.select_gst_Id;
                        for(int i=1; i<=itemsGstId.length;i++){
                            contents[i]=itemsGstId[i-1];
                            log.debug(contents[i]);
                        }
                        be_im_pm_spinnerAdapter = new BE_IM_PM_SpinnerAdapter(context,R.layout.spinner_layout,contents);
                        S_GSTId.setAdapter(be_im_pm_spinnerAdapter);

                        //Replacement
                        contents = new String[itemsReplacable.length+1];
                        contents[0] = resources.select_Replacement_Mode;
                        for(int i=1; i<=itemsReplacable.length;i++){
                            contents[i]=itemsReplacable[i-1];
                            log.debug(contents[i]);
                        }
                        be_im_pm_spinnerAdapter = new BE_IM_PM_SpinnerAdapter(context,R.layout.spinner_layout,contents);
                        S_IsReplaceble.setAdapter(be_im_pm_spinnerAdapter);
                        //Sub cat 1
                        contents = new String[1];
                        contents[0]=resources.select_subCategory1;
                        loadTheSpinner(S_SubCategory1,contents);
                        //Sub cat 2
                        contents = new String[1];
                        contents[0]=resources.select_subCategory2;
                        loadTheSpinner(S_SubCategory2,contents);
                        //Sub cat 3
                        contents = new String[1];
                        contents[0]=resources.select_subCategory3;
                        loadTheSpinner(S_SubCategory3,contents);
                        //Sub cat 4
                        contents = new String[1];
                        contents[0]=resources.select_subCategory4;
                        loadTheSpinner(S_SubCategory4,contents);
                        //Set UI from the input data
                        setUiFromInputData();
                    }
                });
            }
        };
        runOnThread(loadSpinners);
        //Configure display message
        displayMessage.configure(true,2000,100,false,T_MessageView);
        //Enable smooth scroll
        S_Scroll.setSmoothScrollingEnabled(true);
    }
    /**
     * this method is used to addActionListeners to the various UI elements
     */
    private void addActionListeners(){
        //Add action listeners to the category spinner
        S_Category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //check if category is selected or not
                if(position==0){
                    T_LCategory.setVisibility(GONE);
                    loadTheSpinner(S_SubCategory1,new String[]{resources.select_subCategory1});
                    return;
                }
                T_LCategory.setVisibility(View.VISIBLE);
                //Clear the subcat2,3,4
                itemsSubcategory2 = new ArrayList<HashMap<String, String>>();
                itemsSubcategory3 = new ArrayList<HashMap<String, String>>();
                itemsSubcategory4 = new ArrayList<HashMap<String, String>>();
                //Check the selected item and accordingly query subcat1
                String catId=itemsCategory.get(position-1).get(sIDB_CAT_ID);
                loadSpinnerValues(catId,1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        S_SubCategory1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //check if category is selected or not
                if(position==0){
                    //hide label
                    T_LSubcategory1.setVisibility(GONE);
                    //Load defualt in successive categories
                    loadTheSpinner(S_SubCategory2,new String[]{resources.select_subCategory2});
                    return;
                }
                T_LSubcategory1.setVisibility(View.VISIBLE);
                //Clear sub cat 3,4
                itemsSubcategory3 = new ArrayList<HashMap<String, String>>();
                itemsSubcategory4 = new ArrayList<HashMap<String, String>>();
                //Check the selected item and accordingly query subcat1
                String catId=itemsSubcategory1.get(position-1).get(sIDB_CAT_ID);
                loadSpinnerValues(catId,2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        S_SubCategory2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //check if category is selected or not
                if(position==0){
                    T_LSubcategory2.setVisibility(GONE);
                    loadTheSpinner(S_SubCategory3,new String[]{resources.select_subCategory3});
                    return;
                }
                T_LSubcategory2.setVisibility(View.VISIBLE);
                //Clear sub cat 4
                itemsSubcategory4 = new ArrayList<HashMap<String, String>>();
                //Check the selected item and accordingly query subcat1
                String catId=itemsSubcategory2.get(position-1).get(sIDB_CAT_ID);
                loadSpinnerValues(catId,3);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        S_SubCategory3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //check if category is selected or not
                if(position==0){
                    T_LSubcategory3.setVisibility(GONE);
                    loadTheSpinner(S_SubCategory4,new String[]{resources.select_subCategory4});
                    return;
                }
                T_LSubcategory3.setVisibility(View.VISIBLE);
                //Check the selected item and accordingly query subcat1
                String catId=itemsSubcategory3.get(position-1).get(sIDB_CAT_ID);
                loadSpinnerValues(catId,4);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //Spinner subcat 4
        S_SubCategory4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    T_LSubcategory4.setVisibility(GONE);
                    return;
                }
                T_LSubcategory4.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Spinner measuremnt
        S_MeasurementUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    T_Lmeasurementunit.setVisibility(GONE);
                }else
                {
                    T_Lmeasurementunit.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Spinner GST
        S_GSTId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    T_LGstId.setVisibility(GONE);
                }else
                {
                    T_LGstId.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Replace policy
        S_IsReplaceble.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    T_LIsReplaceble.setVisibility(GONE);
                }else
                {
                    T_LIsReplaceble.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Arrow down , scrol screen
        I_arrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Scroll the screen down by 0.75 of new page
                S_Scroll.smoothScrollBy(0,(int)(baseLayout.getMeasuredWidth()*0.20));
            }
        });

        //Arrow backspace
        I_arrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BE_IM_PM_AlertBuilder dialogBox = new BE_IM_PM_AlertBuilder(context);
                dialogBox.createAlertBuilder(resources.updatingProduct,resources.doYouWantToGoBack);
                //Add one more product button
                DialogInterface.OnClickListener goBack= new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //i just wanna exit boy
                        exitModule(-1);
                    }
                };
                dialogBox.addPositiveButton(resources.yes,goBack);
                //No i dont want to exit
                DialogInterface.OnClickListener dontwannaExit= new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //DO NOhting
                    }
                };
                dialogBox.addNegetiveButton(resources.no,dontwannaExit);
                dialogBox.showDialogBox();
            }
        });

        //SUbmit button
        B_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkFields()){
                    //Add the product to the product table
                    new BE_IM_PM_CloseKeyboard().closeTheKeyBoard(context);
                    WriteDataToDb();
                }
            }
        });


    }

    /**
     * This method is used to run a runnable on a back ground thread.
     * @param runnable
     */
    private void runOnThread(Runnable runnable){
        new Thread(runnable).start();
    }

    /**
     * THis method is used to load the spinners
     * @param catId category ID which is previous category of the current sub cat
     * @param subCatLevel 0-cat , 1-Sub cat1 , 4-Sub cat4
     */
    private void loadSpinnerValues(final String catId, final int subCatLevel){
        Runnable loadValues = new Runnable() {
            @Override
            public void run() {
                //progress dialog
                final BE_IM_PM_ProgressDialog be_im_PM_progressDialog = new BE_IM_PM_ProgressDialog(context);
                be_im_PM_progressDialog.createProgressDialog(resources.Loading,resources.Please_Wait);
                //Get the items of cat/sub cat
                String[] contents=null;
                Spinner spinner=null;
                if(subCatLevel==1){
                    itemsSubcategory1 = dbInterface.getAllTheCategories(catId);
                    //Load the sub cat 1
                    contents = new String[itemsSubcategory1.size()+1];
                    contents[0]=resources.select_subCategory1;
                    for(int i=1; i<=itemsSubcategory1.size();i++){
                        contents[i]=itemsSubcategory1.get(i-1).get(sIDB_CAT_NAME);
                        log.debug(contents[i]);
                    }
                    spinner = S_SubCategory1;

                }else if(subCatLevel==2){
                    itemsSubcategory2 = dbInterface.getAllTheCategories(catId);
                    //Load the sub cat 1
                    contents = new String[itemsSubcategory2.size()+1];
                    contents[0]=resources.select_subCategory2;
                    for(int i=1; i<=itemsSubcategory2.size();i++){
                        contents[i]=itemsSubcategory2.get(i-1).get(sIDB_CAT_NAME);
                        log.debug(contents[i]);
                    }
                    spinner = S_SubCategory2;

                }else if(subCatLevel==3){
                    itemsSubcategory3 = dbInterface.getAllTheCategories(catId);
                    //Load the sub cat 1
                    contents = new String[itemsSubcategory3.size()+1];
                    contents[0]=resources.select_subCategory3;
                    for(int i=1; i<=itemsSubcategory3.size();i++){
                        contents[i]=itemsSubcategory3.get(i-1).get(sIDB_CAT_NAME);
                        log.debug(contents[i]);
                    }
                    spinner = S_SubCategory3;
                }else if(subCatLevel==4){
                    itemsSubcategory4 = dbInterface.getAllTheCategories(catId);
                    //Load the sub cat 1
                    contents = new String[itemsSubcategory4.size()+1];
                    contents[0]=resources.select_subCategory4;
                    for(int i=1; i<=itemsSubcategory4.size();i++){
                        contents[i]=itemsSubcategory4.get(i-1).get(sIDB_CAT_NAME);
                        log.debug(contents[i]);
                    }
                    spinner = S_SubCategory4;
                }
                loadTheSpinner(spinner,contents);
                be_im_PM_progressDialog.dismissProgressDialog();
            }
        };
        runOnUiThread(loadValues);
    }

    /**
     * This method is used to laod the spinner
     * @param spinner soinner which is tobe loaded
     * @param contents contents of the spinner
     */
    private void loadTheSpinner(final Spinner spinner, final String[] contents){
        Runnable loadTheSpinner = new Runnable() {
            @Override
            public void run() {
                BE_IM_PM_SpinnerAdapter be_im_pm_spinnerAdapter=new BE_IM_PM_SpinnerAdapter(context,R.layout.spinner_layout,contents);
                if(spinner!=null){
                    spinner.setAdapter(be_im_pm_spinnerAdapter);
                }else{
                    log.debug("loading spinner"+"spinner is null");
                }
            }
        };
        runOnUiThread(loadTheSpinner);
    }

    /**
     * This module will check all the fileds before
     * adding the values to the database.
     * @return true/false , true-- proceed to adding else error message displayed on top
     */
    private boolean checkFields(){
        //Check the name
        String hsnNumber=E_hsnNumber.getText().toString();
        //Check HSN
        if(hsnNumber.equalsIgnoreCase("")){
            //throw error
            displayMessage.displayMessage(resources.pleaseEnterValidHSNNumber, Color.RED);
            return false;
        }
        //Check measurement unit
        int selectedPos = S_MeasurementUnit.getSelectedItemPosition();
        int itemLength = itemsMeasurementUnits.length;
        if((itemLength!=0 && selectedPos==0)){
            displayMessage.displayMessage(resources.pleaseSelectMeasurementUnit,Color.RED);
            return false;
        }

        //Check GST id
        selectedPos = S_GSTId.getSelectedItemPosition();
        itemLength =itemsGstId.length;
        if((itemLength!=0 && selectedPos==0)){
            displayMessage.displayMessage(resources.pleaseSelectGstCode,Color.RED);
            return false;
        }

        //Check replacement mode
        selectedPos = S_IsReplaceble.getSelectedItemPosition();
        itemLength =itemsReplacable.length;
        if((itemLength!=0 && selectedPos==0)){
            displayMessage.displayMessage(resources.pleaseSelectReplaceblePolicy,Color.RED);
            return false;
        }
        return true;
    }

    /**
     * This method creates the hash map which is to be writen to the database
     */
    private void WriteDataToDb(){
        //Hashmap to hold the data
        final HashMap<String,String> product = new HashMap<>();

        //Add part number to the hashmap
        product.put(BE_IM_PM_Constants.addAProductKeyArray[0], constantDataFromInput.get(viewAProductKeyArray[0]));
        //Get and add hsn number
        product.put(BE_IM_PM_Constants.addAProductKeyArray[1],E_hsnNumber.getText().toString());
        //Add cat ID
        product.put(BE_IM_PM_Constants.addAProductKeyArray[2],constantDataFromInput.get(viewAProductKeyArray[2]));
        //Add sub cat1 ID
        product.put(BE_IM_PM_Constants.addAProductKeyArray[3],constantDataFromInput.get(viewAProductKeyArray[3]));
        //Add sub cat2 ID
        product.put(BE_IM_PM_Constants.addAProductKeyArray[4],constantDataFromInput.get(viewAProductKeyArray[4]));
        //Add sub cat3 ID
        product.put(BE_IM_PM_Constants.addAProductKeyArray[5],constantDataFromInput.get(viewAProductKeyArray[5]));
        //Add sub cat4 ID
        product.put(BE_IM_PM_Constants.addAProductKeyArray[6],constantDataFromInput.get(viewAProductKeyArray[6]));
        //GST Tax Code
        product.put(BE_IM_PM_Constants.addAProductKeyArray[7],String.valueOf(S_GSTId.getSelectedItemPosition()));
        //product name
        product.put(BE_IM_PM_Constants.addAProductKeyArray[8],E_productName.getText().toString());
        //Measuring unit
        product.put(BE_IM_PM_Constants.addAProductKeyArray[9],itemsMeasurementUnits[S_MeasurementUnit.getSelectedItemPosition()-1]);
        //Stock
        product.put(BE_IM_PM_Constants.addAProductKeyArray[10],String.valueOf(constantDataFromInput.get(viewAProductKeyArray[10])));
        //Clear stock
        product.put(BE_IM_PM_Constants.addAProductKeyArray[11], String.valueOf(constantDataFromInput.get(viewAProductKeyArray[11])));
        //Bar code
        product.put(BE_IM_PM_Constants.addAProductKeyArray[12],E_barcode.getText().toString());
        //Description
        String description=E_productDescription.getText().toString();
        if(description.length()>128){
            description = description.substring(0,127);
        }
        product.put(BE_IM_PM_Constants.addAProductKeyArray[13],description);
        //Replacement
        int isAllowed=1;
        if(itemsReplacable[S_IsReplaceble.getSelectedItemPosition()-1].equalsIgnoreCase("Allowed")){
            isAllowed=0;
        }
        product.put(BE_IM_PM_Constants.addAProductKeyArray[14],String.valueOf(isAllowed));
        //Tax reserve
        product.put(BE_IM_PM_Constants.addAProductKeyArray[15],(constantDataFromInput.get(viewAProductKeyArray[15])));
        //master tr date
        product.put(BE_IM_PM_Constants.addAProductKeyArray[16],(constantDataFromInput.get(viewAProductKeyArray[16])));

        Runnable writeToDB =  new Runnable() {
            @Override
            public void run() {
                BE_IM_PM_ProgressDialog progressDialog = new BE_IM_PM_ProgressDialog(context);
                progressDialog.createProgressDialog(resources.updatingProduct,resources.Please_Wait);
                //Write to DB
                boolean ret = dbInterface.updateAProduct(product);
                progressDialog.dismissProgressDialog();
                if(ret){
                    //DB write success
                    displayMessage.displayMessage(resources.productUpdatedSuccessfully,Color.GREEN);
                    BE_IM_PM_AlertBuilder dialogBox = new BE_IM_PM_AlertBuilder(context);
                    dialogBox.createAlertBuilder(resources.updatingProduct,resources.productUpdatedSuccessfully);
                    //Add one more product button
                    DialogInterface.OnClickListener okAddOneMore= new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Clear all the fields
                            exitModule(0);
                        }
                    };
                    dialogBox.addPositiveButton(resources.ok,okAddOneMore);
                    dialogBox.showDialogBox();
                }else{
                    //DB Write fail
                    displayMessage.displayMessage(resources.productUpdationFailed,Color.RED);
                }
            }
        };
        runOnThread(writeToDB);
    }

    /**
     * set the input data to the UI
     */
    private void setUiFromInputData(){
        productDetails = new HashMap<>();
        //Get the hashmap data from bundle
        //Get the bundle which has the product details
        Bundle bundle_prodcuctDetails = getIntent().getBundleExtra(BE_IM_PM_Constants.PRODUCT_DETAILS);
        if(bundle_prodcuctDetails!=null){
            //Parse the bundle to a hashmap
            for(int i = 0; i< viewAProductKeyArray.length; i++){
                String val=bundle_prodcuctDetails.getString(viewAProductKeyArray[i]);
                productDetails.put(viewAProductKeyArray[i],val);
                log.debug("prod details"+val);
            }
        }
        //Part number
        constantDataFromInput.put(viewAProductKeyArray[0],productDetails.get(viewAProductKeyArray[0]));
        //name
        constantDataFromInput.put(viewAProductKeyArray[8],productDetails.get(viewAProductKeyArray[8]));
        //cat
        constantDataFromInput.put(viewAProductKeyArray[2],productDetails.get(viewAProductKeyArray[2]));
        //sub cat1
        constantDataFromInput.put(viewAProductKeyArray[3],productDetails.get(viewAProductKeyArray[3]));
        //sub cat 2
        constantDataFromInput.put(viewAProductKeyArray[4],productDetails.get(viewAProductKeyArray[4]));
        //sub cat 3
        constantDataFromInput.put(viewAProductKeyArray[5],productDetails.get(viewAProductKeyArray[5]));
        //sub cat 4
        constantDataFromInput.put(viewAProductKeyArray[6],productDetails.get(viewAProductKeyArray[6]));
        //GST TAx
        constantDataFromInput.put(viewAProductKeyArray[7],productDetails.get(viewAProductKeyArray[7]));
        //Stock
        constantDataFromInput.put(viewAProductKeyArray[10],productDetails.get(viewAProductKeyArray[10]));
        //Clear Stock
        constantDataFromInput.put(viewAProductKeyArray[11],productDetails.get(viewAProductKeyArray[11]));
        //tax resever
        constantDataFromInput.put(viewAProductKeyArray[15],productDetails.get(viewAProductKeyArray[15]));
        //Tax reserve date
        constantDataFromInput.put(viewAProductKeyArray[16],productDetails.get(viewAProductKeyArray[16]));

        E_productName.setText(productDetails.get(viewAProductKeyArray[8]));
        E_productDescription.setText(productDetails.get(viewAProductKeyArray[13]));
        E_barcode.setText(productDetails.get(viewAProductKeyArray[12]));
        E_hsnNumber.setText(productDetails.get(viewAProductKeyArray[1]));
     //   S_Category.setSelection(getSpinnerPosFromCatID(S_Category,productDetails.get(viewAProductKeyArray[2])));
//        S_SubCategory1.setSelection(getSpinnerPosFromCatID(S_SubCategory1,productDetails.get(viewAProductKeyArray[3])));
  //      S_SubCategory2.setSelection(getSpinnerPosFromCatID(S_SubCategory2,productDetails.get(viewAProductKeyArray[4])));
  //      S_SubCategory3.setSelection(getSpinnerPosFromCatID(S_SubCategory3,productDetails.get(viewAProductKeyArray[5])));
  //      S_SubCategory4.setSelection(getSpinnerPosFromCatID(S_SubCategory4,productDetails.get(viewAProductKeyArray[6])));

        S_MeasurementUnit.setSelection(getMeasurementUnitSpinnerPosFromCode(productDetails.get(viewAProductKeyArray[9])));
        S_GSTId.setSelection(GetgstSpinnerPostionFromCode(productDetails.get(viewAProductKeyArray[7])));
        S_IsReplaceble.setSelection(getReplacementPolicySpinnerPosFromCode(productDetails.get(viewAProductKeyArray[14])));
    }

    /**
     * THis method will return the category/sub cat ID from the spinner and its position
     * @param spinner spinner
     * @param pos position of the spinner
     * @return cat ID as String
     */
    private String getCatIdFromSpinnerPos(Spinner spinner,int pos){
        try {
            if (spinner == S_Category) {
                return itemsCategory.get(pos - 1).get(sIDB_CAT_ID);
            }
            if (spinner == S_SubCategory1) {
                return itemsSubcategory1.get(pos - 1).get(sIDB_CAT_ID);
            }
            if (spinner == S_SubCategory2) {
                return itemsSubcategory2.get(pos - 1).get(sIDB_CAT_ID);
            }
            if (spinner == S_SubCategory3) {
                return itemsSubcategory3.get(pos - 1).get(sIDB_CAT_ID);
            }
            if (spinner == S_SubCategory4) {
                return itemsSubcategory4.get(pos - 1).get(sIDB_CAT_ID);
            }
        }catch (Exception e){
            return "0";
        }
        return "0";
    }

    /**
     * this method will return the Spinner position from the category ID from the spinner
     * @param catID cat ID
     * @param spinner spinner
     * @return position of the spinner in int
     */
    private int getSpinnerPosFromCatID(Spinner spinner,String catID){
        if(spinner == S_Category){
            return  getPositionOfCatInArrayList(catID,itemsCategory);
        }
        if(spinner == S_SubCategory1){
            return  getPositionOfCatInArrayList(catID,itemsSubcategory1);
        }
        if(spinner == S_SubCategory2){
            return  getPositionOfCatInArrayList(catID,itemsSubcategory2);
        }
        if(spinner == S_SubCategory3){
            return  getPositionOfCatInArrayList(catID,itemsSubcategory3);
        }
        if(spinner == S_SubCategory4){
            return  getPositionOfCatInArrayList(catID,itemsSubcategory4);
        }
        return 0;
    }

    /**
     * This metod will return the position of the item in the array list from the cat ID
     * @param catId   category ID of the item
     * @param catDetails list of categories
     * @return position of the item in the array list from the cat ID in integer
     */
    private int getPositionOfCatInArrayList(String catId,ArrayList<HashMap<String,String>> catDetails){
        for(int i=0; i<catDetails.size();i++){
           if(catId.equals(catDetails.get(i).get(sIDB_CAT_ID))){
               return i+1;
           }
        }
        return 0;
    }

    /**
     * This method will return the measurent unit spinner position
     * from the measurement unit
     * @param unit measurement unit string
     * @return position
     */
    private int getMeasurementUnitSpinnerPosFromCode(String unit){
        for(int i=0;i<itemsMeasurementUnits.length;i++){
            String temp=(itemsMeasurementUnits[i]);
            log.debug("Measure id: "+unit+" VS "+temp);
            if(unit.equalsIgnoreCase(temp)){
                log.debug("returning position"+i+1);
                return i+1;
            }
        }
        return 0;
    }

    /**
     * THis metod will return the GST spiner position from the GST code
     * @param gstCode gst code
     * @return position of the spinner
     */
    private int GetgstSpinnerPostionFromCode(String gstCode){
        for(int i=0;i<gstdataHashList.size();i++){
            String temp =gstdataHashList.get(i).get(BE_IM_PM_Constants.gstDummyIdKeys[0]);
            log.debug("GST: "+gstCode+" VS "+temp);
            if(gstCode.equalsIgnoreCase(temp)){
                return i+1;
            }
        }
        return 0;
    }

    /**
     * This method will return the postion of the replacement policy spinner
     * from the code
     * @param code replacement policy code
     * @return spiinner position
     */
    private int getReplacementPolicySpinnerPosFromCode(String code){
        if(code.equalsIgnoreCase("false")){
            return 1;
        }else if(code.equalsIgnoreCase("true")){
            return 2;
        }
        return 0;
    }




    /**
     * This module will close the current activity and exit
     */
    private void exitModule(int errorCode){
        // TODO cleared here
        BE_IM_PM_DBInterface.clearInstance();
        BE_IM_PM_Resources.clearInstance();
        BE_IM_PM_Log.clearInstance();
        new BE_IM_PM_CloseKeyboard().closeTheKeyBoard(context);
        this.setResult(errorCode);
        this.finish();
    }
}
