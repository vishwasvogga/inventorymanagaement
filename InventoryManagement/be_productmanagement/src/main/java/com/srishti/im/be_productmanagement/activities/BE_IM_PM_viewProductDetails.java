package com.srishti.im.be_productmanagement.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.srishti.im.be_productmanagement.R;
import com.srishti.im.be_productmanagement.adapters.BE_IM_PM_productDetailsRecyclerAdapter;
import com.srishti.im.be_productmanagement.adapters.LineDividerDecoration;
import com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants;
import com.srishti.im.be_productmanagement.db_middleware.BE_IM_PM_DBInterface;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_CloseKeyboard;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Log;
import com.srishti.im.be_productmanagement.utils.BE_IM_PM_Resources;

import java.util.HashMap;

import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.viewAProductKeyArray;

/**
 * This activity is intended to display all the details of the product
 */

public class BE_IM_PM_viewProductDetails extends Activity {

    private LinearLayout baseLayout=null;
    private RecyclerView RE_productDetails=null;
    private Context context=null;
    private Button B_Back,B_Exit=null;
    private BE_IM_PM_Resources resources=null;
    private HashMap<String,String> productDetails=null;
    private BE_IM_PM_Log log=null;
    private BE_IM_PM_DBInterface dbInterface=null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseLayout=(LinearLayout) getLayoutInflater().inflate(R.layout.viewproductdetailslayout,null);
        setContentView(baseLayout);
        initialise();
        configure();
        addActionListners();
    }

    /**
     * This function is used to initalise all the elements
     * and get the UI refrence
     */
    private void initialise(){
        context=this;
        RE_productDetails = (RecyclerView) getViewById(R.id.RE_VPD_productDetails,baseLayout);
        B_Back=(Button)getViewById(R.id.B_VPD_Back,baseLayout);
        B_Exit=(Button)getViewById(R.id.B_VPD_Exit,baseLayout);
        resources = BE_IM_PM_Resources.getInstance();
        productDetails = new HashMap<>();
        log = BE_IM_PM_Log.getInstance();
        dbInterface = BE_IM_PM_DBInterface.getInstance(context);
    }

    /**
     * Thi method is used to configure all the configuarble values
     */
    private void configure(){
        //Get the bundle which has the product details
        Bundle bundle_prodcuctDetails = getIntent().getBundleExtra(BE_IM_PM_Constants.PRODUCT_DETAILS);
        if(bundle_prodcuctDetails!=null){
            //Parse the bundle to a hashmap
            for(int i = 0; i< viewAProductKeyArray.length; i++){
                String val=bundle_prodcuctDetails.getString(viewAProductKeyArray[i]);
                productDetails.put(viewAProductKeyArray[i],val);
                log.debug("prod details"+val);
            }
        }

        //Configure the recycle view
        BE_IM_PM_productDetailsRecyclerAdapter adapter = new BE_IM_PM_productDetailsRecyclerAdapter(context,
        productDetails,R.layout.recycleproductdetailslayout);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        RE_productDetails.setLayoutManager(linearLayoutManager);
        RE_productDetails.addItemDecoration(new LineDividerDecoration(context));
        RE_productDetails.setAdapter(adapter);
    }

    /**
     * This method retruns the view by id in the base layout
     * @param id id of that perticular view element
     * @param baseLayout this is the base layout in which the views are positioned
     * @return instance of the view
     */
    private View getViewById(int id, View baseLayout){
        return baseLayout.findViewById(id);
    }

    /**
     * This method is used to add the action listners to the various
     * UI elements
     */
    private void addActionListners(){
        B_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitModule(0);
            }
        });

        B_Exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitModule(-1);
            }
        });
    }

    /**
     * This module will close the current activity and exit
     */
    private void exitModule(int errorCode){
        // TODO cleared here
        BE_IM_PM_DBInterface.clearInstance();
        BE_IM_PM_Resources.clearInstance();
        BE_IM_PM_Log.clearInstance();
        new BE_IM_PM_CloseKeyboard().closeTheKeyBoard(context);
        setResult(errorCode);
        this.finish();
    }
}
