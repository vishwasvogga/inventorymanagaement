package com.srishti.im.be_productmanagement.utils;

import android.util.Log;

import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.LOG_TITTLE;
import static com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants.isLogEnabled;

/**
 * THis class provides the logging methods
 */

public class BE_IM_PM_Log {
    private static BE_IM_PM_Log be_im_pm_log=null;

    /**
     * To get the singletoninstance of thei class
     * @return BE_IM_PM_Log
     */
    public static  BE_IM_PM_Log getInstance(){
        if(be_im_pm_log==null){
            be_im_pm_log = new BE_IM_PM_Log();
        }
        return be_im_pm_log;
    }

    /**
     * To clear the previously created singleton instance
     */
    public static void clearInstance(){
        be_im_pm_log = null;
    }

    /**
     * THis metodis used to log the message
     * @param message mesage which is to be logged
     */
    public void debug(String message){
        if(isLogEnabled) {
            Log.d(LOG_TITTLE, message);
        }
    }
}
