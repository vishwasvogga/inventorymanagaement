package com.srishti.im.inventorymanagement;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.srishti.im.be_productmanagement.activities.BE_IM_PM_AddProductPrequel;
import com.srishti.im.be_productmanagement.activities.BE_IM_PM_viewProduct;
import com.srishti.im.be_productmanagement.constants.BE_IM_PM_Constants;

/**
 * Test program main activity
 */

public class Mainactivity_TP extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout baseLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.tpmainact,null);
        setContentView(baseLayout);
        //Add a product
        Button addAProduct = (Button)baseLayout.findViewById(R.id.button1);
        addAProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Enable logging
                BE_IM_PM_Constants.isLogEnabled=true;

                Intent addAProduct= new Intent(Mainactivity_TP.this,BE_IM_PM_AddProductPrequel.class);
                startActivity(addAProduct);
            }
        });

        Button viewProdcut = (Button) baseLayout.findViewById(R.id.button2);
        viewProdcut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Enable logging
                BE_IM_PM_Constants.isLogEnabled=true;

                //Configure view for Viewing the product
                Bundle config = new Bundle();
                config.putInt(BE_IM_PM_Constants.MODE_OF_VIEW_ACT,BE_IM_PM_Constants.VIEW);

                Intent viewProducts= new Intent(Mainactivity_TP.this,BE_IM_PM_viewProduct.class);
                viewProducts.putExtra(BE_IM_PM_Constants.MODE_OF_VIEW_ACT,config);
                startActivity(viewProducts);
            }
        });

        Button editProduct = (Button) baseLayout.findViewById(R.id.button3);
        editProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Enable logging
                BE_IM_PM_Constants.isLogEnabled=true;

                //Configure view for Viewing the product
                Bundle config = new Bundle();
                config.putInt(BE_IM_PM_Constants.MODE_OF_VIEW_ACT,BE_IM_PM_Constants.EDIT);

                Intent viewProducts= new Intent(Mainactivity_TP.this,BE_IM_PM_viewProduct.class);
                viewProducts.putExtra(BE_IM_PM_Constants.MODE_OF_VIEW_ACT,config);
                startActivity(viewProducts);
            }
        });

        Button deleteProduct = (Button) baseLayout.findViewById(R.id.button4);
        deleteProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Enable logging
                BE_IM_PM_Constants.isLogEnabled=true;

                //Configure view for Viewing the product
                Bundle config = new Bundle();
                config.putInt(BE_IM_PM_Constants.MODE_OF_VIEW_ACT,BE_IM_PM_Constants.DELETE);

                Intent viewProducts= new Intent(Mainactivity_TP.this,BE_IM_PM_viewProduct.class);
                viewProducts.putExtra(BE_IM_PM_Constants.MODE_OF_VIEW_ACT,config);
                startActivity(viewProducts);
            }
        });
    }
}
